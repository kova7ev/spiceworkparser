package com.entelo.service.parsers.utils;

import java.util.Map;

public class Response {
	private Map<String, String> mHeaders;
	private String mContent;
	
	public Response(Map<String, String> headers, String response){
		mHeaders = headers;
		mContent = response;
	}
	
	public Map<String, String> getHeaders(){
		return mHeaders;
	}
	
	public String getContent(){
		return mContent;
	}
}
