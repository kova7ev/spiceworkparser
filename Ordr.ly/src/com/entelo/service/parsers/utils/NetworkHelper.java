package com.entelo.service.parsers.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

public class NetworkHelper {
	private static Map<String, String> mDefaultHeaders;
	
	static{
		mDefaultHeaders = new HashMap<String, String>();
		mDefaultHeaders.put("User-Agent", "curl/7.37.0");
		mDefaultHeaders.put("Accept", "*/*");
		mDefaultHeaders.put("X-Requested-With", "XMLHttpRequest");
	}
	
	public static Response sendHttpGetRequest(String url) {
		return sendHttpGetRequest(url, mDefaultHeaders);
	}
	
	public static Response sendHttpGetRequest(String url, Map<String, String> headers) {
		try {
			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(url);

			for (String name : headers.keySet()){
				get.setHeader(name, headers.get(name));
			}

			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();

			StringBuilder builder = new StringBuilder();
			if (entity != null) {
				Scanner scanner = new Scanner(entity.getContent());
				while (scanner.hasNextLine()) {
					builder.append(scanner.nextLine());
				}
				
				Map<String, String> responseHeaders = new HashMap<String, String>();
				for (Header header : response.getAllHeaders()){
					responseHeaders.put(header.getName(), header.getValue());
				}
				return new Response(responseHeaders, builder.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new Response(new HashMap<String, String>(), "");
	}
	
	public static Response sendHttpPostRequest(String url, Map<String, String> data, Map<String, String> headers){
		HttpClient client = new DefaultHttpClient();
	    HttpPost post = new HttpPost(url);
	    
	    for (String name : headers.keySet()){
			post.setHeader(name, headers.get(name));
		}
	    
	    try {
	      StringBuilder builder = new StringBuilder();
	      for (String name : data.keySet()){
	      	  builder.append(String.format("%s=%s", name, data.get(name)));
	      }
	      
	      post.setEntity(new ByteArrayEntity(builder.toString().getBytes("UTF8")));
	
	      HttpResponse response = client.execute(post);
	      HttpEntity entity = response.getEntity();

			builder = new StringBuilder();
			if (entity != null) {
				Scanner scanner = new Scanner(entity.getContent());
				while (scanner.hasNextLine()) {
					builder.append(scanner.nextLine());
				}
				
				Map<String, String> responseHeaders = new HashMap<String, String>();
				for (Header header : response.getAllHeaders()){
					responseHeaders.put(header.getName(), header.getValue());
				}
				return new Response(responseHeaders, builder.toString());
			}


	    } catch (IOException e) {
	      e.printStackTrace();
	    }
		return new Response(new HashMap<String, String>(), "");
	}
}
