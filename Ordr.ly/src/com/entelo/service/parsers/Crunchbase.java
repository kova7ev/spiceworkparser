package com.entelo.service.parsers;

import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

import com.entelo.service.parsers.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class Crunchbase extends Parser{

	public Crunchbase() {

	}

	// returns service ID (username/screen name/user id/etc)
	public String getServiceID(String url) {
		String regexp = "https?://(www\\.|)crunchbase.com/person/(.*)";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(url);

		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return null;
		}
	}

	// does this URL belong to this service
	public boolean belongToService(String url) {
		return getServiceID(url) != null;
	}

	// return the canonical version of the URL
	public String normalizeURL(String url) {
		if (!url.contains("www")) {
			int index = url.indexOf("://");
			return String.format("%swww.%s", url.substring(0, index + 3),
					url.substring(index + 3));
		}
		return url;
	}

	// return parsed page with all attributes on the page
	// additionalValues should get appended to the return results
	public HashMap<String, Object> parseURL(String url,
			ArrayList<Object> additionalValues) {

		HashMap<String, Object> records = new HashMap<String, Object>();

		try {
			Document doc = Jsoup.connect(url).get();

			records.put("profile_image_url", parseProfileImageUrl(doc));
			records.put("other_profiles", parseOtherProfiles(doc));
			records.put("title", parseTitle(doc));
			records.put("born", parseBorn(doc));
			records.put("location", parseLocation(doc));
			records.put("detailed_description", parseDetailedDescription(doc));
			records.put("positions", parsePositions(doc));
			records.put("advisory_roles", parseAdvisoryRoles(doc));
			records.put("educations", parseEducation(doc));
			records.put("investments", parseInvestments(doc, url));
			records.put("news", parseNews(doc, url));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return records;
	}

	private String parseProfileImageUrl(Document doc) {
		Element infoCard = doc.select("div.base.info-card").get(0);
		Element infoCardContent = infoCard.select(
				"div.info-card-content.container").get(0);
		Element logoLinksContainer = infoCardContent.select(
				"div.logo-links-container").get(0);
		Element image = logoLinksContainer.getElementsByTag("img").get(0);
		return image.attr("src");
	}

	private Object parseOtherProfiles(Document doc) {
		Element infoCard = doc.select("div.base.info-card").get(0);
		Element infoCardContent = infoCard.select(
				"div.info-card-content.container").get(0);
		Element logoLinksContainer = infoCardContent.select(
				"div.logo-links-container").get(0);
		Element socialLinks = logoLinksContainer.select("div.social-links")
				.get(0);

		List<Object> profiles = new ArrayList<Object>();
		for (Element li : socialLinks.getElementsByTag("li")) {
			HashMap<String, Object> profileInfo = new HashMap<String, Object>();

			Element a = li.getElementsByTag("a").get(0);
			profileInfo.put("profile", a.attr("class"));
			profileInfo.put("url", a.attr("href"));
			profiles.add(profileInfo);
		}

		return profiles;
	}

	private String parseTitle(Document doc) {
		Element infoCard = doc.select("div.base.info-card").get(0);
		Element infoCardContent = infoCard.select(
				"div.info-card-content.container").get(0);
		Element infoCardOverviewContent = infoCardContent.select(
				"div.info-card-overview-content.container").get(0);
		Element definitionListContainer = infoCardOverviewContent.select(
				"dl.definition-list.container").get(0);

		Elements dt = definitionListContainer.getElementsByTag("dt");
		Elements dd = definitionListContainer.getElementsByTag("dd");

		for (int i = 0; i < dt.size(); i++) {
			String sectionName = dt.get(i).text();

			if (sectionName.equals("Primary:")) {
				return dd.get(i).text();
			}
		}

		return "";
	}

	private String parseBorn(Document doc) {
		Element infoCard = doc.select("div.base.info-card").get(0);
		Element infoCardContent = infoCard.select(
				"div.info-card-content.container").get(0);
		Element infoCardOverviewContent = infoCardContent.select(
				"div.info-card-overview-content.container").get(0);
		Element definitionListContainer = infoCardOverviewContent.select(
				"dl.definition-list.container").get(0);

		Elements dt = definitionListContainer.getElementsByTag("dt");
		Elements dd = definitionListContainer.getElementsByTag("dd");

		for (int i = 0; i < dt.size(); i++) {
			String sectionName = dt.get(i).text();

			if (sectionName.equals("Born:")) {
				return dd.get(i).text();
			}
		}

		return "";
	}

	private String parseLocation(Document doc) {
		Element infoCard = doc.select("div.base.info-card").get(0);
		Element infoCardContent = infoCard.select(
				"div.info-card-content.container").get(0);
		Element infoCardOverviewContent = infoCardContent.select(
				"div.info-card-overview-content.container").get(0);
		Element definitionListContainer = infoCardOverviewContent.select(
				"dl.definition-list.container").get(0);

		Elements dt = definitionListContainer.getElementsByTag("dt");
		Elements dd = definitionListContainer.getElementsByTag("dd");

		for (int i = 0; i < dt.size(); i++) {
			String sectionName = dt.get(i).text();

			if (sectionName.equals("Location:")) {
				return dd.get(i).text();
			}
		}

		return "";
	}

	private String parseDetailedDescription(Element doc) {
		Element timelineCardContainer = doc.select(
				"div.timeline-card-container").get(0);
		Element infoTabDescription = timelineCardContainer.select(
				"div.base.info-tab.description").get(0);
		Element description = infoTabDescription.select(
				"div.card-content.container").get(0);
		String text = description.text().replace(". See MoreSee Less", ""); 
		
		if (!text.contains("Click/Touch UPDATE above to add a Description for")){
			return text;
		}else{
			return "";
		}
	}

	private Object parsePositions(Element doc) {
		Element timelineCardContainer = doc.select(
				"div.timeline-card-container").get(0);
		Element infoTabExperiences = timelineCardContainer.select(
				"div.base.info-tab.experiences").get(0);
		Element cardContentContainer = infoTabExperiences.select(
				"div.card-content.container").get(0);
		List<Object> positions = new ArrayList<Object>();
		for (Element li : cardContentContainer.getElementsByTag("li")) {
			HashMap<String, Object> position = new HashMap<String, Object>();
			Element profile = li.select("span.profile").get(0);
			Element img = profile.getElementsByTag("img").get(0);
			position.put("logo", img.attr("src"));
			Element infoBlock = li.select("div.info-block").get(0);
			Element h4 = infoBlock.getElementsByTag("h4").get(0);
			Element a = h4.getElementsByTag("a").get(0);
			position.put("organization", a.text());
			position.put("src", a.attr("href"));
			Elements h5 = infoBlock.getElementsByTag("h5");
			position.put("title", h5.get(0).text());
			position.put("date", h5.get(1).text());

			positions.add(position);
		}
		return positions;
	}

	private Object parseAdvisoryRoles(Element doc) {
		Element timelineCardContainer = doc.select(
				"div.timeline-card-container").get(0);
		List<Object> roles = new ArrayList<Object>();
		if (timelineCardContainer.select("div.base.info-tab.advisory_roles")
				.size() > 0) {
			Element infoTabAdvisoryRoles = timelineCardContainer.select(
					"div.base.info-tab.advisory_roles").get(0);
			Element cardContentContainer = infoTabAdvisoryRoles.select(
					"div.card-content.container").get(0);
			for (Element li : cardContentContainer.getElementsByTag("li")) {
				HashMap<String, Object> role = new HashMap<String, Object>();
				Element profile = li.select("span.profile").get(0);
				Element img = profile.getElementsByTag("img").get(0);
				role.put("logo", img.attr("src"));
				Element infoBlock = li.select("div.info-block").get(0);
				Element h4 = infoBlock.getElementsByTag("h4").get(0);
				Element a = h4.getElementsByTag("a").get(0);
				role.put("organization", a.text());
				role.put("src", a.attr("href"));
				Elements h5 = infoBlock.getElementsByTag("h5");
				role.put("title", h5.get(0).text());
				role.put("date", h5.get(1).text());

				roles.add(role);
			}
		}

		return roles;
	}

	private Object parseEducation(Element doc) {
		Element timelineCardContainer = doc.select(
				"div.timeline-card-container").get(0);
		Elements tabs = timelineCardContainer
				.select("div.base.info-tab.education");
		
		List<Object> educations = new ArrayList<Object>();
		
		if (tabs.size() > 0) {
			Element infoTabEducation = tabs.get(0);
			Element cardContentContainer = infoTabEducation.select(
					"div.card-content.container").get(0);

			for (Element li : cardContentContainer.getElementsByTag("li")) {
				HashMap<String, Object> education = new HashMap<String, Object>();
				Element profile = li.select("span.profile").get(0);
				Element img = profile.getElementsByTag("img").get(0);
				education.put("logo", img.attr("src"));
				Element infoBlock = li.select("div.info-block").get(0);
				Element h4 = infoBlock.getElementsByTag("h4").get(0);
				Element a = h4.getElementsByTag("a").get(0);
				education.put("organization", a.text());
				education.put("src", a.attr("href"));
				Element actionBlock = infoBlock.select("div.action-block").get(
						0);
				education.put("date", actionBlock.text());

				educations.add(education);
			}
		}
		return educations;
	}

	private Object parseInvestments(Element doc, String url) {
		String investmentsHtml = NetworkHelper.sendHttpGetRequest(String.format("%s/investments",
				url)).getContent();
		Element investmentsDoc = Jsoup.parse(investmentsHtml);
		Element infoTabInvestments = investmentsDoc.select(
				"div.base.info-tab.investments").get(0);
		Element cardContentContainer = infoTabInvestments.select(
				"div.card-content.container").get(0);
		List<Object> investments = new ArrayList<Object>();

		for (Element li : cardContentContainer.getElementsByTag("li")) {
			HashMap<String, Object> investment = new HashMap<String, Object>();
			Element profile = li.select("span.profile").get(0);
			Element img = profile.getElementsByTag("img").get(0);
			investment.put("logo", img.attr("src"));
			Element infoBlock = li.select("div.info-block").get(0);
			Element h4 = infoBlock.getElementsByTag("h4").get(0);
			Element a = h4.getElementsByTag("a").get(0);
			investment.put("organization", a.text());
			investment.put("src", a.attr("href"));
			Element h5 = infoBlock.getElementsByTag("h5").get(0);
			investment.put("description", h5.text());
			Element info = infoBlock.select("h4.investment").get(0);
			investment.put("sum", info.ownText());
			Element series = info.select("span.series").get(0);
			investment.put("Series", series.ownText().replace("/", "").trim());
			Elements spans = series.select("span.with");

			if (spans.size() > 0) {
				Element others = spans.get(0).getElementsByTag("a").get(0);
				String foundinRoundHtml = NetworkHelper.sendHttpGetRequest(String.format(
						"http://www.crunchbase.com%s", others.attr("href"))).getContent();
				Element foundingRoundDoc = Jsoup.parse(foundinRoundHtml);
				Element foundingRoundInfoCard = foundingRoundDoc.select(
						"div.base.info-card").get(0);
				HashMap<String, Object> foundingRound = new HashMap<String, Object>();
				Element foundingRoundContainer = foundingRoundInfoCard.select(
						"div.info-card-content.container").get(0);

				Element definitionListContainer = foundingRoundContainer
						.select("dl.definition-list.container").get(0);
				Elements dt = definitionListContainer.getElementsByTag("dt");
				Elements dd = definitionListContainer.getElementsByTag("dd");

				for (int i = 0; i < dt.size(); i++) {
					String sectionName = dt.get(i).text();

					if (sectionName.equals("Funding Type:")) {
						foundingRound.put("funding_type", dd.get(i).text());
					} else if (sectionName.equals("Announced On:")) {
						foundingRound.put("announced_on", dd.get(i).text());
					} else if (sectionName.equals("Raised:")) {
						// Ignore this because its record available early
					} else if (sectionName.equals("Investors:")) {
						StringBuilder builder = new StringBuilder();
						for (Element item : dd.get(i).getAllElements()) {
							if (!item.text().contains("See All (")) {
								builder.append(item.text());
							}
						}
						foundingRound.put("investors", builder.toString());
					}
				}
				investment.put("founding_round", foundingRound);
			} else {
				investment.put("founding_round", "");
			}
			investments.add(investment);
		}
		return investments;
	}

	private Object parseNews(Element doc, String url) {
		String newsHtml = NetworkHelper.sendHttpGetRequest(String.format("%s/press", url)).getContent();
		Element newsDoc = Jsoup.parse(newsHtml);
		Element infoTabPress = newsDoc.select(
				"div.base.info-tab.press_mentions").get(0);
		Element cardHeaderContainer = infoTabPress.select(
				"div.card-header.container.row").get(0);
		Element headline = cardHeaderContainer.getElementsByTag("h2").get(0);

		String regexp = "News\\s\\((\\d+)\\)";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(headline.text());

		List<Object> news = new ArrayList<Object>();

		if (matcher.find()) {
			int numberNews = Integer.parseInt(matcher.group(1));

			Element cardContentContainer = infoTabPress.select(
					"div.card-content.container").get(0);

			int index = 0;
			int pageNumber = 1;
			for (Element li : cardContentContainer.getElementsByTag("li")) {
				HashMap<String, Object> record = new HashMap<String, Object>();
				Element infoBlock = li.select("div.info-block").get(0);
				Element a = infoBlock.getElementsByTag("a").get(0);
				record.put("title", a.text());
				record.put("href", a.attr("href"));
				news.add(record);
				index++;
			}

			while (index < numberNews) {
				pageNumber++;
				String link = String
						.format("%s/press?page=%d", url, pageNumber);
				newsHtml = NetworkHelper.sendHttpGetRequest(link).getContent();
				newsDoc = Jsoup.parse(newsHtml);
				infoTabPress = newsDoc.select("div.base.info-tab.press_mentions").get(0);
				cardHeaderContainer = infoTabPress.select("div.card-header.container.row").get(0);
				headline = cardHeaderContainer.getElementsByTag("h2").get(0);

				cardContentContainer = infoTabPress.select("div.card-content.container").get(0);
				for (Element li : cardContentContainer.getElementsByTag("li")) {
					HashMap<String, Object> record = new HashMap<String, Object>();
					Element infoBlock = li.select("div.info-block").get(0);
					Element a = infoBlock.getElementsByTag("a").get(0);
					record.put("title", a.text());
					record.put("href", a.attr("href"));
					news.add(record);
					index++;
				}
			}
		}

		return news;
	}
}