package com.entelo.service.parsers;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class Parser {
	// returns service ID (username/screen name/user id/etc)
	public abstract String getServiceID(String url);

	// does this URL belong to this service
	public abstract boolean belongToService(String url);

	// return the canonical version of the URL
	public abstract String normalizeURL(String url);

	// return parsed page with all attributes on the page
	// additionalValues should get appended to the return results
	public abstract HashMap<String, Object> parseURL(String url,
			ArrayList<Object> additionalValues);
	
	// return parsed page with all attributes on the page
	public HashMap<String, Object> parseURL(String url){
		return parseURL(url, new ArrayList<>());
	}
}
