package com.entelo.service.parsers;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.*;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

import com.entelo.service.parsers.utils.NetworkHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;

public class Spiceworks extends Parser {

	public Spiceworks() {

	}

	// returns service ID (username/screen name/user id/etc)
	public String getServiceID(String url) {
		String regexp = "https?://(www\\.|)community.spiceworks.com/people/(.*)";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(url);

		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return null;
		}
	}

	// does this URL belong to this service
	public boolean belongToService(String url) {
		return getServiceID(url) != null;
	}

	// return the canonical version of the URL
	public String normalizeURL(String url) {
		if (!url.contains("www")) {
			int index = url.indexOf("://");
			return String.format("%swww.%s", url.substring(0, index + 3),
					url.substring(index + 3));
		}
		return url;
	}

	// return parsed page with all attributes on the page
	// additionalValues should get appended to the return results
	public HashMap<String, Object> parseURL(String url,
			ArrayList<Object> additionalValues) {

		HashMap<String, Object> records = new HashMap<String, Object>();

		try {
			Document doc = Jsoup.connect(url).get();
			records.put("public_name", parsePublicName(doc));
			records.put("url_name", parseUrlName(doc));
			records.put("location", parseLocation(doc));
			records.put("followers", parseFollowers(doc, url));
			records.put("following", parseFollowing(doc, url));
			records.put("profile_image_url", parseProfileImageUrl(doc));
			records.put("interests", parseInterest(doc));
			records.put("certifications", parseCertifications(doc));
			records.put("name", parseName(doc));
			records.put("title", parseTitle(doc));
			records.put("summary", parseSummary(doc));
			records.put("other_profiles", parseProfiles(doc));

			Document activity = Jsoup.connect(String.format("%s/activity", url)).get();
			records.put("groups", parseGroups(activity, url));
			records.put("activities", parseActivities(activity));

			records.put("url", parseUrl(url));
			records.put("categories", parseCategories(url));
			records.put("projects", parseProjects(url));
			records.put("vendors", parseVendors(url));
		} catch (IOException e) {
			e.printStackTrace();
		}

		return records;
	}

	
	private Object parsePublicName(Element doc) {
		Element headerContent = doc.getElementsByClass("header-content").get(0);
		Element div = headerContent.getElementsByClass("name-and-controls").get(0);
		return div.getElementsByTag("h1").get(0).text();
	}

	private Object parseUrlName(Element doc) {
		Element div = doc.getElementsByClass("section-navbar").get(0);
		Element a = div.getElementsByClass("home").get(0);
		return a.attr("href").replace("/people/", "");
	}

	private Object parseLocation(Element doc) {
		Element div = doc.select("div.company-and-location.profile-info")
				.get(0);
		Element location = div.getElementById("my-location");

		if (location != null) {
			return location.text();
		} else {
			return "";
		}
	}

	private Object parseFollowers(Element doc, String url) {
		List<Object> followersList = new ArrayList<Object>();

		HashMap<String, Object> followers = new HashMap<String, Object>();
		Elements counts = doc.select("div.follower-counts.profile-info").get(0)
				.getElementsByClass("count");
		int number = Integer.parseInt(counts.get(1).text());
		followers.put("number", number);

		int numberPages = (int) Math.ceil((float) number / 25.0f);
		//System.out.println(Math.ceil((float) number / 25.0f));
		int index = url.lastIndexOf("/");
		String name = url.substring(index + 1);
		String regexp = "jQuery\\(\\\"#followers_modal\\s\\.modal-body\"\\).html\\(\\\"(.*)\\\"\\);\\s?ViewHelpers\\.initUserModal\\(\\);";

		for (int i = 0; i < numberPages; i++) {
			String urlPage = String
					.format("http://community.spiceworks.com/people/%s/followers?page=%d&per_page=25",
							name, i + 1);
			String page = NetworkHelper.sendHttpGetRequest(urlPage).getContent();
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(page);
			String html = null;

			if (matcher.find()) {
				html = matcher.group(1).replace("\\n", "\n").replace("\\t", "\t").replace("\\\"", "\"").replace("\\", "");
			}
			
			Document followersPage = Jsoup.parse(html);
			for (Element follower : followersPage.getElementsByTag("li")){
				Element div = follower.getElementsByClass("image").get(0);
				Element img = div.getElementsByTag("img").get(0);
				String followerImgUrl = img.attr("src");
				Element info = follower.getElementsByClass("info").get(0);
				Element a = info.getElementsByTag("a").get(0);
				String followerPublicName = a.text();
				String followerUrl= a.attr("href");
				
				Hashtable<String, Object> followerInfo = new Hashtable<String, Object>();
				followerInfo.put("profile_image_url", followerImgUrl);
				followerInfo.put("public_name", followerPublicName);
				followerInfo.put("url", followerUrl);
				
				followersList.add(followerInfo);
			}
		}

		followers.put("detail", followersList);
		return followers;
	}
	
	private Object parseFollowing(Element doc, String url) {
		List<Object> followingsList = new ArrayList<Object>();

		HashMap<String, Object> followings = new HashMap<String, Object>();
		Elements counts = doc.select("div.follower-counts.profile-info").get(0)
				.getElementsByClass("count");
		int number = Integer.parseInt(counts.get(0).text());
		followings.put("number", number);

		int numberPages = (int) Math.ceil((float) number / 25);
		int index = url.lastIndexOf("/");
		String name = url.substring(index + 1);
		String regexp = "jQuery\\(\\\"#following_modal\\s\\.modal-body\"\\).html\\(\\\"(.*)\\\"\\);\\s?ViewHelpers\\.initUserModal\\(\\);";

		for (int i = 0; i < numberPages; i++) {
			String urlPage = String
					.format("http://community.spiceworks.com/people/%s/following?page=%d&per_page=25",
							name, (i+1));
			String page = NetworkHelper.sendHttpGetRequest(urlPage).getContent();
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(page);
			String html = null;

			if (matcher.find()) {
				html = matcher.group(1).replace("\\n", "\n").replace("\\t", "\t").replace("\\\"", "\"").replace("\\", "");
			}
			
			Document followingsPage = Jsoup.parse(html);
			for (Element following : followingsPage.getElementsByTag("li")){
				Element div = following.getElementsByClass("image").get(0);
				Element img = div.getElementsByTag("img").get(0);
				String followerImgUrl = img.attr("src");
				Element info = following.getElementsByClass("info").get(0);
				Element a = info.getElementsByTag("a").get(0);
				String followingPublicName = a.text();
				String followingUrl= a.attr("href");
				
				Hashtable<String, Object> followingInfo = new Hashtable<String, Object>();
				followingInfo.put("profile_image_url", followerImgUrl);
				followingInfo.put("public_name", followingPublicName);
				followingInfo.put("url", followingUrl);
				
				followingsList.add(followingInfo);
			}
		}

		followings.put("detail", followingsList);
		return followings;
	}

	private Object parseGroups(Element doc, String url) {
		List<Object> groupsList = new ArrayList<Object>();

		HashMap<String, Object> groups = new HashMap<String, Object>();
		int index = url.lastIndexOf("/");
		String name = url.substring(index + 1);
		String regexp = "jQuery\\(\\\"#groups_modal\\s\\.modal-body\"\\).html\\(\\\"(.*)\\\"\\);\\s?ViewHelpers\\.initGroupModal\\(\\);";

		int i = 0;
		while (true) {
			String urlPage = String
					.format("http://community.spiceworks.com/people/%s/groups?page=%d&per_page=25",
							name, (i+1));
			String page = NetworkHelper.sendHttpGetRequest(urlPage).getContent();
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(page);
			String html = null;

			if (matcher.find()) {
				html = matcher.group(1).replace("\\n", "\n").replace("\\t", "\t").replace("\\\"", "\"").replace("\\", "");
			}
			
			if (html.equals("<ul class=\"modal-list group-list\">\n</ul>\n")){
				break;
			}
			
			Document groupsPage = Jsoup.parse(html);
			for (Element group : groupsPage.getElementsByTag("li")){
				Element div = group.getElementsByClass("image").get(0);
				Element img = div.getElementsByTag("img").get(0);
				String groupImgUrl = img.attr("src");
				Element info = group.getElementsByClass("info").get(0);
				Element a = info.getElementsByTag("a").get(0);
				String groupName = a.text();
				String groupUrl= a.attr("href");
				
				Hashtable<String, Object> groupInfo = new Hashtable<String, Object>();
				groupInfo.put("group_image_url", groupImgUrl);
				groupInfo.put("name", groupName);
				groupInfo.put("url", groupUrl);
				
				groupsList.add(groupInfo);
			}
			
			i++;
		}

		groups.put("number", groupsList.size());
		groups.put("detail", groupsList);
		return groups;
	}

	private Object parseVendors(String url) {
		List<Object> vendorsList = new ArrayList<Object>();

		HashMap<String, Object> vendors = new HashMap<String, Object>();
		int index = url.lastIndexOf("/");
		String name = url.substring(index + 1);
		String regexp = "jQuery\\(\\\"#vendors_modal\\s\\.modal-body\"\\).html\\(\\\"(.*)\\\"\\);";

		int i = 0;
		while (true) {
			String urlPage = String
					.format("http://community.spiceworks.com/people/%s/vendors?page=%d&per_page=25",
							name, (i+1));
			String page = NetworkHelper.sendHttpGetRequest(urlPage).getContent();
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(page);
			String html = null;

			if (matcher.find()) {
				html = matcher.group(1).replace("\\n", "\n").replace("\\t", "\t").replace("\\\"", "\"").replace("\\", "");
			}
			
			if (html.equals("<ul class=\"modal-list vendor-list\">\n</ul>\n")){
				break;
			}
			
			Document vendorPage = Jsoup.parse(html);
			for (Element vendor : vendorPage.getElementsByTag("li")){
				Element div = vendor.getElementsByClass("image").get(0);
				Element img = div.getElementsByTag("img").get(0);
				String vendorImgUrl = img.attr("src");
				Element info = vendor.getElementsByClass("info").get(0);
				Element a = info.getElementsByTag("a").get(0);
				String vendorName = a.text();
				String vendorUrl= a.attr("href");
				
				Hashtable<String, Object> vendorInfo = new Hashtable<String, Object>();
				vendorInfo.put("vendor_image_url", vendorImgUrl);
				vendorInfo.put("name", vendorName);
				vendorInfo.put("url", vendorUrl);
				
				vendorsList.add(vendorInfo);
			}
			
			i++;
		}

		vendors.put("number", vendorsList.size());
		vendors.put("detail", vendorsList);
		return vendors;
	}


	private Object parseActivities(Element doc) {
		Element div = doc.getElementsByClass("activity_list").get(0);
		List<Object> activities = new ArrayList<Object>();

		for (Element act : div.getElementsByTag("li")) {
			HashMap<String, Object> activityInfo = new HashMap<String, Object>();
			Element img = act.getElementsByClass("section-avatar").get(0);
			activityInfo.put("section_avatar", img.attr("src"));
			Element content = act.getElementsByClass("activity-content").get(0);
			activityInfo.put("section",content.getElementsByClass("section-link").get(0).text());
			activityInfo.put("name", content.getElementsByClass("user-link").get(0).text());
			Element description = content.getElementsByClass("activity-description").get(0);
			Element a = description.getElementsByTag("a").get(0);
			activityInfo.put("description", a.text());
			activityInfo.put("url", a.attr("href"));
			activities.add(activityInfo);
		}
		return activities;
	}

	private Object parseProfileImageUrl(Element doc) {
		Element a = doc.getElementsByClass("home").get(0);
		Element img = a.getElementsByTag("img").get(0);
		return img.attr("src");
	}

	private Object parseInterest(Element doc) {
		Element div = doc.getElementsByClass("interests-list").get(0);
		StringBuilder builder = new StringBuilder();

		for (Element interest : div.getElementsByTag("li")) {
			builder.append(interest.text());
			builder.append(",");
		}

		String interests = builder.toString();
		return interests.subSequence(0, interests.length() - 1);
	}

	private Object parseCertifications(Element doc) {
		Element headerContent = doc.getElementsByClass("header-content").get(0);
		Element div = headerContent.getElementsByClass("profile-info").get(1);
		Element certifications = div.getElementsByTag("span").get(1);
		return certifications.text();
	}

	private Object parseName(Element doc) {
		Element div = doc.select("div.company-and-location.profile-info")
				.get(0);
		Element name = div.getElementsByTag("span").get(0);
		return name.text();
	}

	private Object parseTitle(Element doc) {
		Element div = doc.select("div.company-and-location.profile-info")
				.get(0);
		Element title = div.getElementsByTag("span").get(1);
		return title.text();
	}

	private Object parseSummary(Element doc) {
		Element div = doc.getElementsByClass("right-quote").get(0);
		return div.text();
	}

	private Object parseProfiles(Element doc) {
		Element div = doc.getElementsByClass("website-list").get(0);
		List<Object> profiles = new ArrayList<Object>();

		for (Element profile : div.getElementsByTag("li")) {
			HashMap<String, Object> profileInfo = new HashMap<String, Object>();
			profileInfo.put(
					"profile",
					profile.getElementsByClass("label").get(0).text()
							.replace(":", ""));
			profileInfo.put("url",
					profile.getElementsByTag("a").get(0).attr("href"));
			profiles.add(profileInfo);
		}
		return profiles;
	}

	private Object parseCategories(String url) {
		try {
			String json = NetworkHelper.sendHttpGetRequest(String.format("%s/technologies.json", url)).getContent();

			HashMap<String, List<Object>> categories = new HashMap<String, List<Object>>();
			JSONObject obj = new JSONObject(String.format("{categories:%s}",
					json));
			JSONArray array = obj.getJSONArray("categories");

			for (int i = 0; i < array.length(); i++) {
				String category = array.getJSONObject(i).getString("name");
				String channel = array.getJSONObject(i).getString(
						"primary_channel");

				if (!categories.containsKey(channel)) {
					categories.put(channel, new ArrayList<Object>());
				}
				categories.get(channel).add(category);
			}
			return categories;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}

	private Object parseUrl(String url) {
		return url;
	}

	private Object parseProjects(String url) {
		try {
			String json = NetworkHelper.sendHttpGetRequest(String.format("%s/projects.json", url)).getContent();

			List<Object> projects = new ArrayList<Object>();
			JSONObject obj = new JSONObject(
					String.format("{projects:%s}", json));

			JSONArray array = obj.getJSONArray("projects");
			for (int i = 0; i < array.length(); i++) {
				String id = array.getJSONObject(i).getString("id");
				String progress = array.getJSONObject(i).getString(
						"in_progress");
				String projectName = array.getJSONObject(i).getString("name");
				String startDate = array.getJSONObject(i).getString(
						"start_date");
				String status = array.getJSONObject(i).getString("status");
				String summary = array.getJSONObject(i).getString("summary");
				String urlName = array.getJSONObject(i).getString("url_name");

				JSONObject authorObj = array.getJSONObject(i).getJSONObject(
						"author");
				String authorName = authorObj.getString("name");
				String authorUrlName = authorObj.getString("url_name");
				String authorPublicName = authorObj.getString("public_name");

				HashMap<String, Object> author = new HashMap<String, Object>();
				author.put("name", authorName);
				author.put("url_name", authorUrlName);
				author.put("public_name", authorPublicName);

				HashMap<String, Object> project = new HashMap<String, Object>();
				project.put("id", id);
				project.put("in_progress", progress);
				project.put("name", projectName);
				project.put("start_date", startDate);
				project.put("status", status);
				project.put("summary", summary);
				project.put("url_name", urlName);
				project.put("author", author);

				projects.add(project);
			}
			return projects;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return "";
	}
}