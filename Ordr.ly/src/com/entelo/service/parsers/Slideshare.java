package com.entelo.service.parsers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.entelo.service.parsers.utils.NetworkHelper;

public class Slideshare extends Parser {

	@Override
	public String getServiceID(String url) {
		String regexp = "https?://(www\\.|)slideshare.net";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(url);

		String path = null;
		
		if (matcher.find()) {
			path = url.replace(matcher.group(0), "");
		} else {
			return null;
		}

		regexp = "/";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(path);
		
		int count = 0;
		while (matcher.find()){
		    count++;
		}

		if (count != 1){
			return null;
		}
		
		regexp = "https?://(www\\.|)slideshare.net/([^/].*)";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(url);

		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return null;
		}

	}

	@Override
	public boolean belongToService(String url) {
		return getServiceID(url) != null;
	}

	@Override
	public String normalizeURL(String url) {
		if (!url.contains("www")) {
			int index = url.indexOf("://");
			return String.format("%swww.%s", url.substring(0, index + 3),
					url.substring(index + 3));
		}
		return url;
	}

	@Override
	public HashMap<String, Object> parseURL(String url,
			ArrayList<Object> additionalValues) {

		HashMap<String, Object> records = new HashMap<String, Object>();

		try {
			Document doc = Jsoup.connect(url).get();

			records.put("profile_image_url", parseProfileImage(doc));
			records.put("name", parseName(doc));
			records.put("location", parseLocation(doc));
			records.put("title", parseWork(doc));
			records.put("industry", parseIndustry(doc));
			records.put("description", parseDescription(doc));

			HashMap<String, Object> profiles = new HashMap<String, Object>();
			profiles.put("website", parseWebsite(doc));
			profiles.put("twitter", parseTwitter(doc));
			profiles.put("linkedin", parseLinkedin(doc));

			records.put("other_profiles", profiles);
			records.put("followers", parseFollowers(doc, url));
			records.put("followings", parseFollowings(doc, url));
			records.put("tags", parseTags(doc));
			records.put("presentations", parsePresentations(doc, url));
			records.put("documents", parseDocuments(doc, url));
			records.put("infographics", parseInfographics(doc, url));
			records.put("favorites", parseFavorites(doc, url));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return records;
	}
	
	private String parseProfileImage(Element doc) {
		Element container = doc.select("div.user-image-container").first();
		Element img = container.getElementsByTag("img").first();
		return img.attr("src").replace("//", "");
	}

	private String parseName(Element doc) {
		Element vcard = doc.select("div.v-card").first();
		Element name = vcard.select("h1.notranslate.fn").first();
		return name.text();
	}

	private String parseLocation(Element doc) {
		Element profileData = doc.select("ul.v-card.profileData").first();
		Element address = profileData.select("ul.adr").first();
		return address.text();
	}

	private String parseWork(Element doc) {
		Element profileData = doc.select("ul.v-card.profileData").first();
		Element work = profileData.select("span.workWrapper.notranslate")
				.first();
		return work.text().replace("Work", "").trim();
	}

	private String parseIndustry(Element doc) {
		Element profileData = doc.select("ul.v-card.profileData").first();
		Element industry = profileData.select("span.spriteProfile.industry")
				.first();
		return industry.text().replace("Industry", "").trim();
	}

	private String parseDescription(Element doc) {
		Element profileData = doc.select("ul.v-card.profileData").first();
		Element description = profileData
				.select("span.description.notranslate").first();
		return description.text();
	}

	private String parseWebsite(Element doc) {
		Element profileData = doc.select("ul.v-card.profileData").first();
		Element website = profileData.select("span.spriteProfile.web").first();
		return website.text();
	}

	private String parseTwitter(Element doc) {
		Element twitter = doc.select("a.notranslate.spriteProfile.twitter")
				.first();
		return twitter.attr("href");
	}

	private String parseLinkedin(Element doc) {
		Element linkedin = doc.select("a.notranslate.spriteProfile.linkedin")
				.first();
		return linkedin.attr("href");
	}

	private Object parseFollowers(Element doc, String url) {
		String html = NetworkHelper.sendHttpGetRequest(String.format("%s/followers", url)).getContent();
		Document followersDoc = Jsoup.parse(html);

		ArrayList<Object> followers = new ArrayList<Object>();

		if (followersDoc.select("div.pagination").size() == 0) {
			HashMap<String, Object> result = new HashMap<>();
			result.put("size", followers.size());
			result.put("detail", followers);
			return result.toString();
		}

		Element pagnation = followersDoc.select("div.pagination").first();
		Elements li = pagnation.getElementsByTag("li");
		String numberPageString = li.get(li.size() - 2).text();
		int numberPage = Integer.parseInt(numberPageString);

		for (int i = 1; i <= numberPage; i++) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/followers/%d", url, i)).getContent();
			Document pageDoc = Jsoup.parse(page);

			Element ul = pageDoc.select("ul.userList").first();
			for (Element user : ul.getElementsByTag("li")) {
				HashMap<String, Object> follower = new HashMap<String, Object>();

				if (user.getElementsByTag("img").size() > 0) {
					Element img = user.getElementsByTag("img").first();
					follower.put("profile_image_url", img.attr("src").replace("//", ""));
					follower.put("url",String.format("http://www.slideshare.net/%s",img.attr("alt")));
				}

				followers.add(follower);
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", followers.size());
		result.put("detail", followers);
		return result.toString();
	}

	private Object parseFollowings(Element doc, String url) {
		String html = NetworkHelper.sendHttpGetRequest(String.format("%s/following", url)).getContent();
		Document followingDoc = Jsoup.parse(html);

		ArrayList<Object> followings = new ArrayList<Object>();

		if (followingDoc.select("div.pagination").size() == 0) {
			HashMap<String, Object> result = new HashMap<>();
			result.put("size", followings.size());
			result.put("detail", followings);
			return result.toString();
		}

		Element pagnation = followingDoc.select("div.pagination").first();
		Elements li = pagnation.getElementsByTag("li");
		String numberPageString = li.get(li.size() - 2).text();
		int numberPage = Integer.parseInt(numberPageString);

		for (int i = 1; i <= numberPage; i++) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/following/%d", url, i)).getContent();
			Document pageDoc = Jsoup.parse(page);

			Element ul = pageDoc.select("ul.userList").first();
			for (Element user : ul.getElementsByTag("li")) {
				HashMap<String, Object> following = new HashMap<String, Object>();

				if (user.getElementsByTag("img").size() > 0) {
					Element img = user.getElementsByTag("img").first();
					following.put("profile_image_url", img.attr("src").replace("//", ""));
					following.put("url", String.format("http://www.slideshare.net/%s", img.attr("alt")));
				}

				followings.add(following);
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", followings.size());
		result.put("detail", followings);
		return result.toString();
	}

	private Object parseTags(Element doc) {
		ArrayList<String> tags = new ArrayList<String>();
		Element tagsMore = doc.getElementById("tagsMore");
		Element span = tagsMore.select("span.notranslate.tagsWrapper").first();

		for (int i = 1; i < span.getElementsByTag("span").size(); i++) {
			Element tag = span.getElementsByTag("span").get(i);
			tags.add(tag.text());
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", tags.size());
		result.put("detail", tags);
		return result;
	}

	private Object parsePresentations(Element doc, String url) {
		boolean lastPage = false;
		int itemPerPage = 20;
		int pageNumber = 0;

		ArrayList<Object> presentations = new ArrayList<Object>();

		while (!lastPage) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/slideshelf/load_more_slideshows?slideshow_type=presentations&offset=%d", url, pageNumber * itemPerPage)).getContent();
			pageNumber++;
			
			try {
				JSONObject json = new JSONObject(page);
				lastPage = !json.getBoolean("has_more_presentations");

				JSONArray array = json.getJSONArray("presentations");
				for (int i = 0; i < array.length(); i++) {
					JSONObject record = array.getJSONObject(i).getJSONObject("slideshow");
					HashMap<String, Object> presentation = new HashMap<String, Object>();

					presentation.put("url",String.format("%s/%s", url, record.getString("stripped_title")));
					presentation.put("thumbnail_image_url",record.getString("thumbnail_image").replace("//",""));
					presentation.put("title", record.getString("title").toString().replaceAll("[^a-zA-Z ]+",""));
					presentation.put("description",record.getString("description").toString().replaceAll("[^a-zA-Z ]+",""));
					presentation.put("total_slides",record.getInt("total_slides"));
					presentation.put("tag_text", record.getString("tag_text").toString().replaceAll("[^a-zA-Z ]+",""));

					presentations.add(presentation);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", presentations.size());
		result.put("detail", presentations);
		return result.toString();
	}

	private Object parseDocuments(Element doc, String url) {
		boolean lastPage = false;
		int itemPerPage = 20;
		int pageNumber = 0;

		ArrayList<Object> documents = new ArrayList<Object>();

		while (!lastPage) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/slideshelf/load_more_slideshows?slideshow_type=documents&offset=%d", url, pageNumber * itemPerPage)).getContent();
			pageNumber++;
			
			try {
				JSONObject json = new JSONObject(page);
				lastPage = !json.getBoolean("has_more_documents");

				JSONArray array = json.getJSONArray("documents");
				for (int i = 0; i < array.length(); i++) {
					JSONObject record = array.getJSONObject(i).getJSONObject("slideshow");
					HashMap<String, Object> document = new HashMap<String, Object>();

					document.put("url",String.format("%s/%s", url, record.getString("stripped_title")));
					document.put("thumbnail_image_url",record.getString("thumbnail_image").replace("//",""));
					document.put("title", record.getString("title").replaceAll("[^a-zA-Z ]+",""));
					document.put("description",record.getString("description").replaceAll("[^a-zA-Z ]+",""));
					document.put("total_slides",record.getInt("total_slides"));
					document.put("tag_text", record.getString("tag_text").replaceAll("[^a-zA-Z ]+",""));

					documents.add(document);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", documents.size());
		result.put("detail", documents);
		return result;
	}

	private Object parseInfographics(Element doc, String url) {
		boolean lastPage = false;
		int itemPerPage = 20;
		int pageNumber = 0;

		ArrayList<Object> infographics = new ArrayList<Object>();

		while (!lastPage) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/slideshelf/load_more_slideshows?slideshow_type=infographics&offset=%d", url, pageNumber * itemPerPage)).getContent();
			pageNumber++;
			
			try {
				JSONObject json = new JSONObject(page);
				lastPage = !json.getBoolean("has_more_infographics");

				JSONArray array = json.getJSONArray("infographics");
				for (int i = 0; i < array.length(); i++) {
					JSONObject record = array.getJSONObject(i).getJSONObject("slideshow");
					HashMap<String, Object> infographic = new HashMap<String, Object>();

					infographic.put("url",String.format("%s/%s", url, record.getString("stripped_title")));
					infographic.put("thumbnail_image_url",record.getString("thumbnail_image").replace("//",""));
					infographic.put("title", record.getString("title").replaceAll("[^a-zA-Z ]+",""));
					infographic.put("description",record.getString("description").replaceAll("[^a-zA-Z ]+",""));
					infographic.put("total_slides",record.getInt("total_slides"));
					infographic.put("tag_text", record.getString("tag_text").replaceAll("[^a-zA-Z ]+",""));

					infographics.add(infographic);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", infographics.size());
		result.put("detail", infographics);
		return result;
	}
	
	private Object parseFavorites(Element doc, String url) {
		String html = NetworkHelper.sendHttpGetRequest(String.format("%s/favorites", url)).getContent();
		Document favoritesDoc = Jsoup.parse(html);

		ArrayList<Object> favorites = new ArrayList<Object>();

		if (favoritesDoc.select("div.pagination").size() == 0) {
			HashMap<String, Object> result = new HashMap<>();
			result.put("size", favorites.size());
			result.put("detail", favorites);
			return result.toString();
		}

		Element pagnation = favoritesDoc.select("div.pagination").first();
		Elements li = pagnation.getElementsByTag("li");
		String numberPageString = li.get(li.size() - 2).text();
		int numberPage = Integer.parseInt(numberPageString);

		for (int i = 1; i <= numberPage; i++) {
			String page = NetworkHelper.sendHttpGetRequest(String.format("%s/favorites/%d", url, i)).getContent();
			Document pageDoc = Jsoup.parse(page);

			Element ul = pageDoc.select("ul.thumbnailFollowGrid").first();
			for (Element user : ul.getElementsByTag("li")) {
				HashMap<String, Object> favorite = new HashMap<String, Object>();

				if (user.getElementsByTag("img").size() > 0) {
					Element img = user.getElementsByTag("img").first();
					Element a = user.getElementsByTag("a").first();
					favorite.put("thumbnail_image_url", img.attr("src").replace("//", ""));
					favorite.put("slideshare_page", String.format("http://www.slideshare.net/%s", a.attr("href")));
					favorite.put("title", a.attr("title").replaceAll("[^a-zA-Z ]+",""));
				}

				favorites.add(favorite);
			}
		}

		HashMap<String, Object> result = new HashMap<>();
		result.put("size", favorites.size());
		result.put("detail", favorites);
		return result.toString();
	}

}
