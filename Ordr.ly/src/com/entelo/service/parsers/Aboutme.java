package com.entelo.service.parsers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.entelo.service.parsers.utils.NetworkHelper;
import com.entelo.service.parsers.utils.Response;

public class Aboutme extends Parser {

	private JSONObject mProfileJson;
	private JSONObject mImagesJson;

	@Override
	public String getServiceID(String url) {
		String regexp = "https?://(www\\.|)about.me";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(url);

		String path = null;

		if (matcher.find()) {
			path = url.replace(matcher.group(0), "");
		} else {
			return null;
		}

		regexp = "/";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(path);

		int count = 0;
		while (matcher.find()) {
			count++;
		}

		if (count != 1) {
			return null;
		}

		regexp = "https?://(www\\.|)about.me/([^/].*)";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(url);

		if (matcher.find()) {
			return matcher.group(2);
		} else {
			return null;
		}

	}

	@Override
	public boolean belongToService(String url) {
		return getServiceID(url) != null;
	}

	@Override
	public String normalizeURL(String url) {
		if (!url.contains("www")) {
			int index = url.indexOf("://");
			return String.format("%swww.%s", url.substring(0, index + 3),
					url.substring(index + 3));
		}
		return url;
	}

	@Override
	public HashMap<String, Object> parseURL(String url,
			ArrayList<Object> additionalValues) {

		HashMap<String, Object> records = new HashMap<String, Object>();

		try {
			Document doc = Jsoup.connect(url).get();
			records.put("summary", parseSummary(doc));
			records.put("biography", parseBiography(doc));
			records.put("screen_name", parseUserName(url));
			records.put("given_name", parseFirstName(url));
			records.put("family_name", parseLastName(url));
			records.put("name", parseDisplayName(url));
			records.put("possessive_name", parsePossessiveDisplayName(url));
			records.put("possessive_given_name", parsePossessiveFirstName(url));
			records.put("twitter_name", parseTwitterName(url));
			records.put("instagram_id", parseInstagramId(url));
			records.put("linkedin_id", parseLinkedinId(url));
			records.put("background_image_url", parseBackgroundImage(url));
			records.put("profile_image_url", parseProfileImage(url));
			records.put("location", parseLocations(url));
			records.put("works", parseWorks(url));
			records.put("education", parseEducations(url));
			records.put("tags", parseTags(url));

			records.put("contact_info", parseContactInfo(url));
			records.put("groups", parseGroups(url));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return records;
	}

	private String parseUserName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("user_name")){
				return mProfileJson.getString("user_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseFirstName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("first_name")){
				return mProfileJson.getString("first_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseLastName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("last_name")){
				return mProfileJson.getString("last_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseDisplayName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("display_name")){
				return mProfileJson.getString("display_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	private String parsePossessiveDisplayName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("possessive_display_name")){
				return mProfileJson.getString("possessive_display_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parsePossessiveFirstName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("possessive_first_name")){
				return mProfileJson.getString("possessive_first_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseTwitterName(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("twitter_name")){
				return mProfileJson.getString("twitter_name");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseInstagramId(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("instagram_id")){
				return mProfileJson.getString("instagram_id");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseLinkedinId(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mProfileJson.has("linkedin_id")){
				return mProfileJson.getString("linkedin_id");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseBackgroundImage(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mImagesJson == null){
				mImagesJson = mProfileJson.getJSONObject("images");
			}
			
			if (mImagesJson.has("background_image")){
				return mImagesJson.getString("background_image");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}
	
	private String parseProfileImage(String url) {
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			if (mImagesJson == null){
				mImagesJson = mProfileJson.getJSONObject("images");
			}
			
			if (mImagesJson.has("profile_image")){
				return mImagesJson.getString("profile_image");
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "";
	}

	private Object parseGroups(String url) {
		List<Object> groups = new ArrayList<Object>();

		try {
			JSONObject json = new JSONObject(getCollectionsJson(String.format(
					"%s/collections", url)));

			if (json.getBoolean("success")) {
				JSONArray groupsArray = json.getJSONArray("groups");

				for (int i = 0; i < groupsArray.length(); i++) {
					Map<String, Object> group = new HashMap<String, Object>();
					List<Object> users = new ArrayList<Object>();

					JSONObject groupObject = groupsArray.getJSONObject(i);
					group.put("id", groupObject.getString("_id"));
					group.put("name", groupObject.getString("display"));
					group.put("url", String.format("%s/collections/%s", url, groupObject.getString("name")));
					group.put("created", groupObject.getString("created"));
					group.put("owner", groupObject.getString("owner"));

					JSONArray usersArray = groupObject.getJSONArray("users");
					for (int j = 0; j < usersArray.length(); j++) {
						Map<String, Object> user = new HashMap<String, Object>();

						JSONObject userObject = usersArray.getJSONObject(j);
						user.put("added", userObject.getString("added"));
						user.put("screen_name", userObject.getString("name"));
						user.put("url", String.format("http://about.me/%s", userObject.getString("name")));

						JSONArray name = userObject.getJSONArray("display");
						if (name.length() == 2) {
							user.put("given_name", name.getString(0));
							user.put("family_name", name.getString(1));
						}

						users.add(user);
					}

					group.put("users", wrapList(users));
					groups.add(group);
				}
			}

		} catch (JSONException e) {
			e.printStackTrace();
		}

		return wrapList(groups);
	}

	private Object parseTags(String url) {
		List<Object> tags = new ArrayList<Object>();

		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			JSONArray jsonTags = mProfileJson.getJSONArray("tags");
			for (int i = 0; i < jsonTags.length(); i++) {
				tags.add(jsonTags.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return wrapList(tags);
	}
	
	private Object parseContactInfo(String url){
		Map<String, Object> contactInfo = new HashMap<String, Object>();
		
		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			contactInfo.put("phone", mProfileJson.has("contact.phone") ? mProfileJson.getString("contact.phone"): "");
			contactInfo.put("country", mProfileJson.has("contact.country") ? mProfileJson.getString("contact.country") : "");
			contactInfo.put("zip", mProfileJson.has("contact.zip") ? mProfileJson.getString("contact.zip") : "");
			contactInfo.put("state", mProfileJson.has("contact.state") ? mProfileJson.getString("contact.state") : "");
			contactInfo.put("city", mProfileJson.has("contact.city") ? mProfileJson.getString("contact.city") : "");
			contactInfo.put("address1", mProfileJson.has("contact.address1") ? mProfileJson.getString("contact.address1") : "");
			contactInfo.put("address2", mProfileJson.has("contact.address2") ? mProfileJson.getString("contact.address2") : "");

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return contactInfo;
	}

	private Object parseEducations(String url) {
		List<Object> educations = new ArrayList<Object>();

		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			JSONArray jsonEducation = mProfileJson.getJSONArray("education");
			for (int i = 0; i < jsonEducation.length(); i++) {
				educations.add(jsonEducation.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return wrapList(educations);
	}

	private Object parseWorks(String url) {
		List<Object> works = new ArrayList<Object>();

		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			JSONArray jsonWork = mProfileJson.getJSONArray("work");
			for (int i = 0; i < jsonWork.length(); i++) {
				works.add(jsonWork.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return wrapList(works);
	}

	private Object parseLocations(String url) {
		List<Object> locations = new ArrayList<Object>();

		try {
			if (mProfileJson == null) {
				mProfileJson = new JSONObject(getProfileJson(url));
			}
			
			JSONArray jsonLocations = mProfileJson.getJSONArray("locations");
			for (int i = 0; i < jsonLocations.length(); i++) {
				locations.add(jsonLocations.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}

		return wrapList(locations);

	}

	private Map<String, Object> wrapList(List<Object> array) {
		Map<String, Object> result = new HashMap<>();
		result.put("size", array.size());
		result.put("detail", array);
		return result;
	}

	private String parseSummary(Element doc) {
		Element profileHead = doc.select("div.profile-head").first();
		Element headline = profileHead.select("h2.headline").first();
		return headline == null ? "" : headline.text();
	}

	private String parseBiography(Element doc) {
		Element biographyContainer = doc.select("div.bio-container.clearfix")
				.first();
		Element biography = biographyContainer.select("div.bio").first();
		return biography.text();
	}

	// TODO: Need optimize and clean
	private String getProfileJson(String url) {
		String html = NetworkHelper.sendHttpGetRequest(url).getContent();

		String regexp = "//<!\\[CDATA\\[define\\('data/user', function\\(\\) \\{\\tvar settings = (.*)//\\]\\]";
		Pattern pattern = Pattern.compile(regexp);
		Matcher matcher = pattern.matcher(html);

		String content = null;

		if (matcher.find()) {
			content = matcher.group(1).replace("//<![CDATA[", "")
					.replace("//]]>", "")
					.replace("return {		Settings: settings	}});", "");
		}

		regexp = "\\{(.*)\\}(.*)";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(content);

		String json = null;
		String params = null;

		if (matcher.find() && matcher.groupCount() == 2) {
			json = matcher.group(1);
			params = matcher.group(2);
		}

		StringBuilder builder = new StringBuilder();
		for (String param : params.split(";")) {
			String record = param.trim();
			if (record.contains("settings.")) {
				String[] terms = record.split("=");
				String paramName = terms[0].replace("settings.", "").trim();
				String paramValue = terms[1].trim();

				builder.append(String
						.format(",\t%s: %s", paramName, paramValue));
			}
		}

		String jsonString = String.format("{%s %s}", json, builder.toString()
				.replace("//", ""));
		String jsonNormalized = new String(jsonString);

		regexp = "[,\\{]([^,:]*:)";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(jsonString);

		List<String> names = new ArrayList<String>();
		while (matcher.find()) {
			String key = matcher.group(1).trim();

			if (!names.contains(key)) {
				names.add(key);
				jsonNormalized = jsonNormalized.replace(
						String.format("\t%s", key),
						String.format(" \"%s\":", key.replace(":", "").trim()));
			}
		}

		regexp = "([a-zA-Z]+'s) ";
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(jsonNormalized);

		Map<String, String> replace = new HashMap<String, String>();
		while (matcher.find()) {
			String word = matcher.group(1);
			replace.put(word.replace("\'", "#"), word);
			jsonNormalized = jsonNormalized.replace(word,
					word.replace("\'", "#"));
		}

		jsonNormalized = jsonNormalized.replace("\'", "\"");
		for (String word : replace.keySet()) {
			jsonNormalized = jsonNormalized.replace(word, replace.get(word));
		}
		return jsonNormalized;
	}

	private String getCollectionsJson(String url) {
		Response response = NetworkHelper.sendHttpGetRequest(url);

		if (response.getHeaders().containsKey("Set-cookie")) {
			String cookie = response.getHeaders().get("Set-cookie");

			String regexp = "pumpkinhead=([^;]*);";
			Pattern pattern = Pattern.compile(regexp);
			Matcher matcher = pattern.matcher(cookie);

			String pumpkinhead = null;

			if (matcher.find()) {
				pumpkinhead = matcher.group(1);
			}

			regexp = "<input name=\"_authentication_token\" type=\"hidden\" value=\"([^\"]*)\" />";
			pattern = Pattern.compile(regexp);
			matcher = pattern.matcher(response.getContent());

			String authenticationToken = null;

			if (matcher.find()) {
				authenticationToken = matcher.group(1);
			}

			Map<String, String> headers = new HashMap<String, String>();
			headers.put("Accept",
					"application/json, text/javascript, */*; q=0.01");
			headers.put("Accept-Language", "en-US,en;q=0.5");
			headers.put("Connection", "keep-alive");
			headers.put("Content-Type",
					"application/x-www-form-urlencoded; charset=UTF-8");
			headers.put("Cookie", String.format("pumpkinhead=%s;", pumpkinhead));
			headers.put("Host", "about.me");
			headers.put("Pragma", "no-cache");
			headers.put("Referer", url);
			headers.put("X-Requested-With", "XMLHttpRequest");

			Map<String, String> data = new HashMap<String, String>();
			int index = url.replace("/collections", "").lastIndexOf("/");
			data.put("owner", String.format("%s&_authentication_token=%s", url
					.replace("/collections", "").substring(index + 1).trim(),
					authenticationToken));

			return NetworkHelper.sendHttpPostRequest(
					"http://about.me/ajax/load_groups", data, headers)
					.getContent();
		}

		return "{}";
	}
}