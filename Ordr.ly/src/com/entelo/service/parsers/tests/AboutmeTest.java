package com.entelo.service.parsers.tests;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.entelo.service.parsers.*;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

import junit.framework.TestCase;

public class AboutmeTest extends TestCase{
    private Aboutme aboutme;
    private String url;
    private Document document;

    @Override
    protected void setUp() throws Exception {
    	aboutme = new Aboutme();
    	url = "http://about.me/jessica_tenny";
    	document = Jsoup.connect(url).get();
    }
    
    public void testGetServiceID() throws Exception {
        assertEquals("jessica_tenny", aboutme.getServiceID("http://www.about.me/jessica_tenny"));
        assertEquals("jessica_tenny", aboutme.getServiceID("https://www.about.me/jessica_tenny"));
        assertEquals("jessica_tenny", aboutme.getServiceID("http://about.me/jessica_tenny"));
        assertEquals(null, aboutme.getServiceID("http://www.about.me/OH_MAN_LETS_CHANGE_OUR_URLS/jessica_tenny"));
    }

    public void testBelongToService() throws Exception {
        assert(aboutme.belongToService("http://www.about.me/jessica_tenny"));
        assert(aboutme.belongToService("https://about.me/jessica_tenny"));
        assertFalse(aboutme.belongToService("www.YAHOO.com/jessica_tenny"));
    }

    public void testNormalizeURL() throws Exception {
        assertEquals(
            "http://www.about.me/jessica_tenny",
            aboutme.normalizeURL("http://about.me/jessica_tenny")
        );
    }

    public void testParseURL() throws Exception {
    	assertEquals(	
        		"{tags={detail=[Technology, Startups, Public Relations, PR, Dance, Foreign Languages, Kickboxing], size=7}, summary=Tech PR at Brew. Kickboxer. Village Dweller., location={detail=[New York NY], size=1}, background_image_url=http://d13pix9kaak6wt.cloudfront.net/background/users/j/e/s/jessica_tenny_1381197417_65.jpg, contact_info={zip=10012, phone=201-519-3526, state=New York, address1=, address2=, city=New York, country=United States}, education={detail=[Colgate University], size=1}, given_name=Jessica, biography=I do Technology PR @ Brew Media Relations in NYC. I live in Greenwich Village and am always on the look out for new places to explore in the city. I'm particularly enthusiastic about crepes and stand-up comedy. Other fun facts - I've lived in Spain, love dancing & kickboxing, and I have a bathtub in my kitchen., works={detail=[Brew Media Relations], size=1}, twitter_name=jesstenny, linkedin_id=9kVsE5_2wM, possessive_given_name=Jessica’s, name=Jessica Tenny, instagram_id=183243096, family_name=Tenny, screen_name=jessica_tenny, possessive_name=Jessica Tenny’s, profile_image_url=http://d13pix9kaak6wt.cloudfront.net/profile/users/j/e/s/jessica_tenny_1408331651_7.jpg, groups={detail=[{id=jessica_tenny/brewpr, users={detail=[{added=Tue Oct 08 2013 02:21:16 +0000, family_name=Sonneland, screen_name=hsonneland, given_name=Haley, url=http://about.me/hsonneland}, {added=Tue Oct 08 2013 02:21:33 +0000, family_name=Hammerling, screen_name=brookehammerling, given_name=Brooke, url=http://about.me/brookehammerling}, {added=Tue Oct 08 2013 02:22:08 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}, {added=Tue Oct 08 2013 02:22:13 +0000, family_name=Carpanzano, screen_name=caitlyncarpanzano, given_name=Caitlyn, url=http://about.me/caitlyncarpanzano}, {added=Tue Oct 08 2013 02:22:29 +0000, family_name=Cook, screen_name=denacook, given_name=Dena, url=http://about.me/denacook}, {added=Tue Oct 08 2013 02:23:03 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Tue Oct 08 2013 02:23:30 +0000, family_name=Prichard, screen_name=meghanprichard, given_name=Meghan, url=http://about.me/meghanprichard}, {added=Tue Oct 08 2013 02:23:51 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Wed Oct 23 2013 18:49:42 +0000, family_name=Kim, screen_name=michellerkim, given_name=Michelle, url=http://about.me/michellerkim}, {added=Wed Oct 23 2013 18:49:43 +0000, family_name=Akselrud, screen_name=tracyakselrud, given_name=Tracy, url=http://about.me/tracyakselrud}, {added=Wed Oct 23 2013 18:49:44 +0000, family_name=Wauck, screen_name=kwauck, given_name=Kate, url=http://about.me/kwauck}, {added=Wed Oct 23 2013 18:49:52 +0000, family_name=Tenny, screen_name=jessica_tenny, given_name=Jessica, url=http://about.me/jessica_tenny}, {added=Wed Oct 23 2013 18:50:23 +0000, family_name=Geffen, screen_name=nikkigeffen, given_name=Nikki, url=http://about.me/nikkigeffen}, {added=Wed Oct 23 2013 18:50:24 +0000, family_name=Villanueva, screen_name=yvonnevillanueva, given_name=Yvonne, url=http://about.me/yvonnevillanueva}, {added=Wed Oct 23 2013 18:50:27 +0000, family_name=Caswell, screen_name=racheal_caswell, given_name=Racheal, url=http://about.me/racheal_caswell}, {added=Wed Oct 23 2013 18:50:29 +0000, family_name= Champion, screen_name=allisonchampion, given_name=Allison, url=http://about.me/allisonchampion}, {added=Wed Oct 23 2013 18:50:33 +0000, family_name=Linn, screen_name=dlinn1090, given_name=Dana, url=http://about.me/dlinn1090}, {added=Fri Apr 11 2014 04:08:21 +0000, family_name=Klosterman, screen_name=courtney.klosterman, given_name=Courtney, url=http://about.me/courtney.klosterman}, {added=Tue Aug 19 2014 02:50:02 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=19}, created=Tue Oct 08 2013 01:53:01 +0000, name=Brew PR, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/brewpr}, {id=jessica_tenny/colgate, users={detail=[{added=Tue Oct 08 2013 02:24:59 +0000, family_name= Raymond, screen_name=hraymond, given_name=Harry, url=http://about.me/hraymond}], size=1}, created=Tue Oct 08 2013 02:24:29 +0000, name=Colgate, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/colgate}, {id=jessica_tenny/cool, users={detail=[{added=Mon Sep 16 2013 20:25:13 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Wed Jul 02 2014 15:31:35 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=2}, created=Mon Sep 16 2013 20:25:13 +0000, name=Cool Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/cool}, {id=jessica_tenny/facebookfriends, users={detail=[{added=Tue Sep 03 2013 18:26:14 +0000, family_name= Tomaeno, screen_name=elliotpr, given_name=Elliot, url=http://about.me/elliotpr}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=TORTORA, screen_name=carmentortora, given_name=CARMEN, url=http://about.me/carmentortora}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Finnegan, screen_name=justinfinnegan, given_name=Justin, url=http://about.me/justinfinnegan}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Blount, screen_name=hagan, given_name=Hagan, url=http://about.me/hagan}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Rygielski, screen_name=marekr, given_name=Marek, url=http://about.me/marekr}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Walton        , screen_name=elizabethwalton, given_name=Liz, url=http://about.me/elizabethwalton}, {added=Mon Feb 03 2014 20:47:06 +0000, family_name=Mukherjee, screen_name=aparnamuk, given_name=Aparna, url=http://about.me/aparnamuk}], size=9}, created=Tue Sep 03 2013 18:26:13 +0000, name=Facebook Friends, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/facebookfriends}, {id=jessica_tenny/greatpage, users={detail=[{added=Fri Jan 24 2014 20:23:31 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}], size=1}, created=Fri Jan 24 2014 20:23:31 +0000, name=Great Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/greatpage}, {id=jessica_tenny/greatpic, users={detail=[{added=Sun Dec 22 2013 16:21:51 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Mon Mar 03 2014 22:20:46 +0000, family_name=Arredondo, screen_name=elianaa, given_name=Eliana, url=http://about.me/elianaa}, {added=Tue Mar 11 2014 17:59:27 +0000, family_name=Vlaskalic, screen_name=gianlucavlaskalic, given_name=Gianluca, url=http://about.me/gianlucavlaskalic}, {added=Tue Aug 19 2014 18:40:40 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}], size=4}, created=Sun Dec 22 2013 16:21:51 +0000, name=Pages with Great Pics, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/greatpic}, {id=jessica_tenny/impressed, users={detail=[{added=Thu Nov 21 2013 01:54:09 +0000, family_name=VanBibber, screen_name=brittanyvanbibber, given_name=Brittany, url=http://about.me/brittanyvanbibber}, {added=Wed Jul 02 2014 17:46:42 +0000, family_name=Sacco, screen_name=shelby_sacco, given_name=Shelby, url=http://about.me/shelby_sacco}], size=2}, created=Thu Nov 21 2013 01:54:09 +0000, name=Impressive Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/impressed}, {id=jessica_tenny/love, users={detail=[{added=Thu Sep 19 2013 03:50:55 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Dec 17 2013 15:56:51 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=2}, created=Thu Sep 19 2013 03:50:55 +0000, name=Pages I Love, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/love}, {id=jessica_tenny/twitterfriends, users={detail=[{added=Tue Sep 03 2013 18:26:31 +0000, family_name=Crowley, screen_name=dens, given_name=Dennis, url=http://about.me/dens}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Malik, screen_name=om, given_name=Om , url=http://about.me/om}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name=Tweney, screen_name=dylan20, given_name=Dylan, url=http://about.me/dylan20}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Limongello, screen_name=tomlimongello, given_name=Tom, url=http://about.me/tomlimongello}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Olanoff, screen_name=drew, given_name=Drew, url=http://about.me/drew}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Scoble, screen_name=scobleizer, given_name=Robert, url=http://about.me/scobleizer}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Gannes, screen_name=lizgannes, given_name=Liz, url=http://about.me/lizgannes}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Bradshaw, screen_name=timbr, given_name=Tim, url=http://about.me/timbr}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Silva, screen_name=christs, given_name=Chris, url=http://about.me/christs}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Wauters, screen_name=robinwauters, given_name=Robin, url=http://about.me/robinwauters}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Snell, screen_name=jsnell, given_name=Jason, url=http://about.me/jsnell}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McLoughlin, screen_name=robmcloughlin, given_name=Rob, url=http://about.me/robmcloughlin}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Biggs, screen_name=johnbiggs, given_name=John, url=http://about.me/johnbiggs}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Perez, screen_name=sarahperez, given_name=Sarah, url=http://about.me/sarahperez}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Steele, screen_name=lock, given_name=Lockhart, url=http://about.me/lock}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=, screen_name=adland, given_name=Adland, url=http://about.me/adland}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McCarthy, screen_name=carolinemccarthy, given_name=Caroline, url=http://about.me/carolinemccarthy}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Hesseldahl, screen_name=ahess247, given_name=Arik, url=http://about.me/ahess247}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Ingram, screen_name=mathewingram, given_name=Mathew, url=http://about.me/mathewingram}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Tai, screen_name=billtai, given_name=Bill, url=http://about.me/billtai}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Sullivan, screen_name=dsullivan, given_name=Danny, url=http://about.me/dsullivan}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= De Rosa, screen_name=anthonyderosa, given_name=Anthony, url=http://about.me/anthonyderosa}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Peretti, screen_name=jonah, given_name=Jonah, url=http://about.me/jonah}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lieb, screen_name=rebeccalieb, given_name=Rebecca, url=http://about.me/rebeccalieb}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Berkowitz, screen_name=dberkowitz, given_name=David, url=http://about.me/dberkowitz}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Bilton, screen_name=nickbilton, given_name=Nick, url=http://about.me/nickbilton}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Schafer, screen_name=ian, given_name=Ian, url=http://about.me/ian}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Thomas, screen_name=owenthomas, given_name=Owen, url=http://about.me/owenthomas}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Maverick, screen_name=kmaverick, given_name=Kristin, url=http://about.me/kmaverick}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lacy, screen_name=sarahlacy, given_name=Sarah, url=http://about.me/sarahlacy}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Morrissey, screen_name=bmorrissey, given_name=Brian, url=http://about.me/bmorrissey}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=ha, screen_name=thepeterha, given_name=peter, url=http://about.me/thepeterha}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Sterling, screen_name=gregsterling, given_name=Greg, url=http://about.me/gregsterling}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Shine, screen_name=benshine, given_name=Benjamin, url=http://about.me/benshine}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Clayman, screen_name=gclayman, given_name=Greg, url=http://about.me/gclayman}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Wilhelm, screen_name=alexwilhelm, given_name=Alex, url=http://about.me/alexwilhelm}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Jean, screen_name=dorothyjean, given_name=Dorothy, url=http://about.me/dorothyjean}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Tepley, screen_name=tepley, given_name=Allison, url=http://about.me/tepley}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lazerow, screen_name=lazerow, given_name=Michael, url=http://about.me/lazerow}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Galloway, screen_name=scottgalloway, given_name=Scott, url=http://about.me/scottgalloway}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Van Grove, screen_name=jbruin, given_name=Jennifer, url=http://about.me/jbruin}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Parr, screen_name=benparr, given_name=Benjamin E., url=http://about.me/benparr}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Walsh, screen_name=mark_walsh, given_name=Mark, url=http://about.me/mark_walsh}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Kaufman, screen_name=michakaufman, given_name=Micha, url=http://about.me/michakaufman}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Steel , screen_name=emilysteel, given_name=Emily, url=http://about.me/emilysteel}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=, screen_name=nmcglynn, given_name=Nick, url=http://about.me/nmcglynn}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Eldon, screen_name=eric.eldon, given_name=Eric Rosser, url=http://about.me/eric.eldon}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Hammerling, screen_name=brookehammerling, given_name=Brooke, url=http://about.me/brookehammerling}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Chen, screen_name=spencerchen, given_name=Spencer, url=http://about.me/spencerchen}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Gallop, screen_name=cindygallop, given_name=Cindy, url=http://about.me/cindygallop}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Thione, screen_name=thione, given_name=Lorenzo, url=http://about.me/thione}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Baratz, screen_name=mbaratz, given_name=Maya, url=http://about.me/mbaratz}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Barber, screen_name=nickbarber, given_name=Nick, url=http://about.me/nickbarber}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Yeung, screen_name=kenyeung, given_name=Ken, url=http://about.me/kenyeung}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Tracte, screen_name=stwo, given_name=Stuart, url=http://about.me/stwo}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McKenzie, screen_name=hamish, given_name=Hamish, url=http://about.me/hamish}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Haot, screen_name=rachelsterne, given_name=Rachel, url=http://about.me/rachelsterne}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Lawler, screen_name=ryanlawler, given_name=Ryan, url=http://about.me/ryanlawler}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Warren, screen_name=christina, given_name=Christina, url=http://about.me/christina}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Shampine, screen_name=mjshampine, given_name=Matthew, url=http://about.me/mjshampine}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Wortham , screen_name=jennydeluxe, given_name=Jenna, url=http://about.me/jennydeluxe}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Teicher, screen_name=aerocles, given_name=David, url=http://about.me/aerocles}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Finnegan, screen_name=justinfinnegan, given_name=Justin, url=http://about.me/justinfinnegan}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Popper, screen_name=benpopper, given_name=Ben, url=http://about.me/benpopper}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Gannett, screen_name=emilygannett, given_name=Emily, url=http://about.me/emilygannett}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Kovach, screen_name=stevekovach, given_name=Steve, url=http://about.me/stevekovach}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Tong, screen_name=briantong, given_name=Brian, url=http://about.me/briantong}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Muradali, screen_name=sashamuradali, given_name=Sasha H., url=http://about.me/sashamuradali}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Schonfeld, screen_name=erickschonfeld, given_name=Erick, url=http://about.me/erickschonfeld}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Akselrud, screen_name=tracyakselrud, given_name=Tracy, url=http://about.me/tracyakselrud}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Tsotsis, screen_name=alexiatsotsis, given_name=Alexia, url=http://about.me/alexiatsotsis}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Waxler, screen_name=cwaxler, given_name=Caroline, url=http://about.me/cwaxler}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Isaac, screen_name=mike.isaac, given_name=Mike, url=http://about.me/mike.isaac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Johnson, screen_name=jcjohnson, given_name=Jason, url=http://about.me/jcjohnson}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Chowdhury, screen_name=rezac, given_name=Reza, url=http://about.me/rezac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Schulte, screen_name=erinschulte, given_name=Erin, url=http://about.me/erinschulte}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Huffington, screen_name=ariannahuffington, given_name=Arianna, url=http://about.me/ariannahuffington}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Brody, screen_name=jacobbrody, given_name=Jacob, url=http://about.me/jacobbrody}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Vogel, screen_name=neilvogel, given_name=Neil, url=http://about.me/neilvogel}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Lazar, screen_name=justjon, given_name=Jon, url=http://about.me/justjon}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fitzpatrick, screen_name=alexjfitzpatrick, given_name=Alex, url=http://about.me/alexjfitzpatrick}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Blount, screen_name=hagan, given_name=Hagan, url=http://about.me/hagan}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Kim, screen_name=oryankim, given_name=Ryan , url=http://about.me/oryankim}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Battelle, screen_name=johnbattelle, given_name=John, url=http://about.me/johnbattelle}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Jarvey, screen_name=nataliejarvey, given_name=Natalie, url=http://about.me/nataliejarvey}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Wohlsen, screen_name=marcuswohlsen, given_name=Marcus, url=http://about.me/marcuswohlsen}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Hightower, screen_name=takihigh, given_name=Takiyah, url=http://about.me/takihigh}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Del Rey, screen_name=delrey, given_name=Jason, url=http://about.me/delrey}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Arrington, screen_name=mike, given_name=Michael, url=http://about.me/mike}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Gallagher, screen_name=davidfg, given_name=David, url=http://about.me/davidfg}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Biddle, screen_name=samfbiddle, given_name=Sam, url=http://about.me/samfbiddle}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Faircloth, screen_name=kellyfaircloth, given_name=Kelly, url=http://about.me/kellyfaircloth}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Granzella Larssen, screen_name=adrian.granzella.larssen, given_name=Adrian, url=http://about.me/adrian.granzella.larssen}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Carpanzano, screen_name=caitlyncarpanzano, given_name=Caitlyn, url=http://about.me/caitlyncarpanzano}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Dellinger, screen_name=ajdellinger, given_name=AJ, url=http://about.me/ajdellinger}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Dalton, screen_name=ddalton, given_name=Doug, url=http://about.me/ddalton}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fiegerman, screen_name=sfiegerman, given_name=Seth, url=http://about.me/sfiegerman}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=, screen_name=conduityoursite, given_name=Conduit, url=http://about.me/conduityoursite}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= MacMillan, screen_name=dougmac, given_name=Doug, url=http://about.me/dougmac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Nice Rob, screen_name=naughtybutnicerob, given_name=Naughty But, url=http://about.me/naughtybutnicerob}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Heussner, screen_name=kimaeheussner, given_name=Ki Mae, url=http://about.me/kimaeheussner}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Bonnington, screen_name=redgirl, given_name=Christina, url=http://about.me/redgirl}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fried, screen_name=ina, given_name=Ina, url=http://about.me/ina}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Griffith, screen_name=eringriffith, given_name=Erin, url=http://about.me/eringriffith}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Carr, screen_name=austincarr, given_name=Austin, url=http://about.me/austincarr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Yarow, screen_name=jay.yarow, given_name=Jay, url=http://about.me/jay.yarow}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Kern, screen_name=elizakern, given_name=Eliza, url=http://about.me/elizakern}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Cook, screen_name=denacook, given_name=Dena, url=http://about.me/denacook}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Segall, screen_name=lauriesegall, given_name=Laurie, url=http://about.me/lauriesegall}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Barna, screen_name=kathbarna, given_name=Katherine, url=http://about.me/kathbarna}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Constine, screen_name=joshconstine, given_name=Josh, url=http://about.me/joshconstine}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Indvik, screen_name=laurenindvik, given_name=Lauren, url=http://about.me/laurenindvik}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Warzel, screen_name=charliewarzel, given_name=Charlie, url=http://about.me/charliewarzel}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Lunden, screen_name=ingrid.lunden, given_name=Ingrid, url=http://about.me/ingrid.lunden}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Raymond, screen_name=hraymond, given_name=Harry, url=http://about.me/hraymond}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Goode, screen_name=laurengoode, given_name=Lauren, url=http://about.me/laurengoode}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Carney, screen_name=mcarney, given_name=Michael, url=http://about.me/mcarney}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Banda, screen_name=jackieb3, given_name=Jackie, url=http://about.me/jackieb3}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Dickey, screen_name=meganrosedickey, given_name=Megan Rose, url=http://about.me/meganrosedickey}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Rick, screen_name=christophorrick, given_name=Christophor, url=http://about.me/christophorrick}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Glenn, screen_name=devonglenn, given_name=Devon, url=http://about.me/devonglenn}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Wang, screen_name=jenniferwang, given_name=Jennifer, url=http://about.me/jenniferwang}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Prichard, screen_name=meghanprichard, given_name=Meghan, url=http://about.me/meghanprichard}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Campisano, screen_name=kcampisano, given_name=Katie, url=http://about.me/kcampisano}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Walton        , screen_name=elizabethwalton, given_name=Liz, url=http://about.me/elizabethwalton}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Sonneland, screen_name=hsonneland, given_name=Haley, url=http://about.me/hsonneland}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Whitfield, screen_name=aaron.whitfield, given_name=Aaron, url=http://about.me/aaron.whitfield}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Ahuja, screen_name=maneet, given_name=Maneet, url=http://about.me/maneet}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Rosario, screen_name=elisabethmrosario, given_name=Elisabeth, url=http://about.me/elisabethmrosario}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=, screen_name=aboutmeteam, given_name=About.me, url=http://about.me/aboutmeteam}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Stone, screen_name=brittanystone, given_name=Brittany, url=http://about.me/brittanystone}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Hamburger, screen_name=ellishamburger, given_name=Ellis, url=http://about.me/ellishamburger}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Hockenson, screen_name=laurenhockenson, given_name=Lauren, url=http://about.me/laurenhockenson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Bea, screen_name=francisybea, given_name=Francis, url=http://about.me/francisybea}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=thompson, screen_name=cadiethompson, given_name=cadie, url=http://about.me/cadiethompson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Hammerling, screen_name=haley_hammerling, given_name=Haley, url=http://about.me/haley_hammerling}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Farr, screen_name=christinafarr, given_name=Christina, url=http://about.me/christinafarr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Minshew, screen_name=kminshew, given_name=Kathryn, url=http://about.me/kminshew}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Colao, screen_name=jjcolao, given_name=J.J., url=http://about.me/jjcolao}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Tomaeno, screen_name=elliotpr, given_name=Elliot, url=http://about.me/elliotpr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=DeAmicis, screen_name=cdeamicis, given_name=Carmel, url=http://about.me/cdeamicis}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Cohen Blatter, screen_name=lucy.blatter, given_name=Lucy, url=http://about.me/lucy.blatter}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Wauck, screen_name=kwauck, given_name=Kate, url=http://about.me/kwauck}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Jackson, screen_name=joseph_jackson, given_name=Joseph, url=http://about.me/joseph_jackson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Smith, screen_name=kevinlsmith, given_name=Kevin, url=http://about.me/kevinlsmith}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=fuller, screen_name=kiyomifuller, given_name=kiyomi, url=http://about.me/kiyomifuller}], size=151}, created=Tue Sep 03 2013 18:26:28 +0000, name=Twitter Friends, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/twitterfriends}, {id=jessica_tenny/wellwritten, users={detail=[{added=Tue Dec 24 2013 04:34:36 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Wed Jan 08 2014 03:16:43 +0000, family_name=Shenoy, screen_name=julianshenoy, given_name=Julian, url=http://about.me/julianshenoy}, {added=Fri Apr 11 2014 04:08:14 +0000, family_name=Klosterman, screen_name=courtney.klosterman, given_name=Courtney, url=http://about.me/courtney.klosterman}, {added=Tue Aug 19 2014 18:40:12 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=4}, created=Tue Dec 24 2013 04:34:36 +0000, name=Pages with Awesome Bios, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/wellwritten}], size=10}}",
        		aboutme.parseURL(url, new ArrayList<Object>()).toString());
    }
    
    public void testParseSummary(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseSummary", Element.class);
			method.setAccessible(true);
	    	String summary = (String) method.invoke(aboutme, document);
	    	assertEquals(summary, "Tech PR at Brew. Kickboxer. Village Dweller.");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseBiography(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseBiography", Element.class);
			method.setAccessible(true);
	    	String biography = (String) method.invoke(aboutme, document);
	    	assertEquals(biography, "I do Technology PR @ Brew Media Relations in NYC. I live in Greenwich Village and am always on the look out for new places to explore in the city. I'm particularly enthusiastic about crepes and stand-up comedy. Other fun facts - I've lived in Spain, love dancing & kickboxing, and I have a bathtub in my kitchen.");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseUserName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseUserName", String.class);
			method.setAccessible(true);
	    	String userName = (String) method.invoke(aboutme, url);
	    	assertEquals(userName, "jessica_tenny");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseFirstName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseFirstName", String.class);
			method.setAccessible(true);
	    	String firstName = (String) method.invoke(aboutme, url);
	    	assertEquals(firstName, "Jessica");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseLastName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseLastName", String.class);
			method.setAccessible(true);
	    	String lastName = (String) method.invoke(aboutme, url);
	    	assertEquals(lastName, "Tenny");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseDisplayName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseDisplayName", String.class);
			method.setAccessible(true);
	    	String displayName = (String) method.invoke(aboutme, url);
	    	assertEquals(displayName, "Jessica Tenny");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParsePossessiveDisplayName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parsePossessiveDisplayName", String.class);
			method.setAccessible(true);
	    	String possessiveDisplayName = (String) method.invoke(aboutme, url);
	    	assertEquals(possessiveDisplayName, "Jessica Tenny’s");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParsePossessiveFirstName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parsePossessiveFirstName", String.class);
			method.setAccessible(true);
	    	String possessiveFirstName = (String) method.invoke(aboutme, url);
	    	assertEquals(possessiveFirstName, "Jessica’s");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseTwitterName(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseTwitterName", String.class);
			method.setAccessible(true);
	    	String twitterName = (String) method.invoke(aboutme, url);
	    	assertEquals(twitterName, "jesstenny");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseInstagramId(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseInstagramId", String.class);
			method.setAccessible(true);
	    	String instagramId = (String) method.invoke(aboutme, url);
	    	assertEquals(instagramId, "183243096");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseLinkedinId(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseLinkedinId", String.class);
			method.setAccessible(true);
	    	String linkedinId = (String) method.invoke(aboutme, url);
	    	assertEquals(linkedinId, "9kVsE5_2wM");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseBackgroundImage(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseBackgroundImage", String.class);
			method.setAccessible(true);
	    	String backgroundImage = (String) method.invoke(aboutme, url);
	    	assertEquals(backgroundImage, "http://d13pix9kaak6wt.cloudfront.net/background/users/j/e/s/jessica_tenny_1381197417_65.jpg");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseProfileImage(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseProfileImage", String.class);
			method.setAccessible(true);
	    	String profileImage = (String) method.invoke(aboutme, url);
	    	assertEquals(profileImage, "http://d13pix9kaak6wt.cloudfront.net/profile/users/j/e/s/jessica_tenny_1408331651_7.jpg");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseWorks(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseWorks", String.class);
			method.setAccessible(true);
	    	String works = method.invoke(aboutme, url).toString();
	    	assertEquals(works, "{detail=[Brew Media Relations], size=1}");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseEducations(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseEducations", String.class);
			method.setAccessible(true);
	    	String educations = method.invoke(aboutme, url).toString();
	    	assertEquals(educations, "{detail=[Colgate University], size=1}");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseTags(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseTags", String.class);
			method.setAccessible(true);
	    	String tags = method.invoke(aboutme, url).toString();
	    	assertEquals(tags, "{detail=[Technology, Startups, Public Relations, PR, Dance, Foreign Languages, Kickboxing], size=7}");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseContactInfo(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseContactInfo", String.class);
			method.setAccessible(true);
	    	String contactInfo = method.invoke(aboutme, url).toString();
	    	assertEquals(contactInfo, "{zip=10012, phone=201-519-3526, state=New York, address1=, address2=, city=New York, country=United States}");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public void testParseGroups(){
		try {
			Method method = aboutme.getClass().getDeclaredMethod("parseGroups", String.class);
			method.setAccessible(true);
	    	String groups = method.invoke(aboutme, url).toString();
	    	assertEquals(groups, "{detail=[{id=jessica_tenny/brewpr, users={detail=[{added=Tue Oct 08 2013 02:21:16 +0000, family_name=Sonneland, screen_name=hsonneland, given_name=Haley, url=http://about.me/hsonneland}, {added=Tue Oct 08 2013 02:21:33 +0000, family_name=Hammerling, screen_name=brookehammerling, given_name=Brooke, url=http://about.me/brookehammerling}, {added=Tue Oct 08 2013 02:22:08 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}, {added=Tue Oct 08 2013 02:22:13 +0000, family_name=Carpanzano, screen_name=caitlyncarpanzano, given_name=Caitlyn, url=http://about.me/caitlyncarpanzano}, {added=Tue Oct 08 2013 02:22:29 +0000, family_name=Cook, screen_name=denacook, given_name=Dena, url=http://about.me/denacook}, {added=Tue Oct 08 2013 02:23:03 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Tue Oct 08 2013 02:23:30 +0000, family_name=Prichard, screen_name=meghanprichard, given_name=Meghan, url=http://about.me/meghanprichard}, {added=Tue Oct 08 2013 02:23:51 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Wed Oct 23 2013 18:49:42 +0000, family_name=Kim, screen_name=michellerkim, given_name=Michelle, url=http://about.me/michellerkim}, {added=Wed Oct 23 2013 18:49:43 +0000, family_name=Akselrud, screen_name=tracyakselrud, given_name=Tracy, url=http://about.me/tracyakselrud}, {added=Wed Oct 23 2013 18:49:44 +0000, family_name=Wauck, screen_name=kwauck, given_name=Kate, url=http://about.me/kwauck}, {added=Wed Oct 23 2013 18:49:52 +0000, family_name=Tenny, screen_name=jessica_tenny, given_name=Jessica, url=http://about.me/jessica_tenny}, {added=Wed Oct 23 2013 18:50:23 +0000, family_name=Geffen, screen_name=nikkigeffen, given_name=Nikki, url=http://about.me/nikkigeffen}, {added=Wed Oct 23 2013 18:50:24 +0000, family_name=Villanueva, screen_name=yvonnevillanueva, given_name=Yvonne, url=http://about.me/yvonnevillanueva}, {added=Wed Oct 23 2013 18:50:27 +0000, family_name=Caswell, screen_name=racheal_caswell, given_name=Racheal, url=http://about.me/racheal_caswell}, {added=Wed Oct 23 2013 18:50:29 +0000, family_name= Champion, screen_name=allisonchampion, given_name=Allison, url=http://about.me/allisonchampion}, {added=Wed Oct 23 2013 18:50:33 +0000, family_name=Linn, screen_name=dlinn1090, given_name=Dana, url=http://about.me/dlinn1090}, {added=Fri Apr 11 2014 04:08:21 +0000, family_name=Klosterman, screen_name=courtney.klosterman, given_name=Courtney, url=http://about.me/courtney.klosterman}, {added=Tue Aug 19 2014 02:50:02 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=19}, created=Tue Oct 08 2013 01:53:01 +0000, name=Brew PR, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/brewpr}, {id=jessica_tenny/colgate, users={detail=[{added=Tue Oct 08 2013 02:24:59 +0000, family_name= Raymond, screen_name=hraymond, given_name=Harry, url=http://about.me/hraymond}], size=1}, created=Tue Oct 08 2013 02:24:29 +0000, name=Colgate, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/colgate}, {id=jessica_tenny/cool, users={detail=[{added=Mon Sep 16 2013 20:25:13 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Wed Jul 02 2014 15:31:35 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=2}, created=Mon Sep 16 2013 20:25:13 +0000, name=Cool Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/cool}, {id=jessica_tenny/facebookfriends, users={detail=[{added=Tue Sep 03 2013 18:26:14 +0000, family_name= Tomaeno, screen_name=elliotpr, given_name=Elliot, url=http://about.me/elliotpr}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=TORTORA, screen_name=carmentortora, given_name=CARMEN, url=http://about.me/carmentortora}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Finnegan, screen_name=justinfinnegan, given_name=Justin, url=http://about.me/justinfinnegan}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Blount, screen_name=hagan, given_name=Hagan, url=http://about.me/hagan}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Rygielski, screen_name=marekr, given_name=Marek, url=http://about.me/marekr}, {added=Tue Sep 03 2013 18:26:14 +0000, family_name= Walton        , screen_name=elizabethwalton, given_name=Liz, url=http://about.me/elizabethwalton}, {added=Mon Feb 03 2014 20:47:06 +0000, family_name=Mukherjee, screen_name=aparnamuk, given_name=Aparna, url=http://about.me/aparnamuk}], size=9}, created=Tue Sep 03 2013 18:26:13 +0000, name=Facebook Friends, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/facebookfriends}, {id=jessica_tenny/greatpage, users={detail=[{added=Fri Jan 24 2014 20:23:31 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}], size=1}, created=Fri Jan 24 2014 20:23:31 +0000, name=Great Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/greatpage}, {id=jessica_tenny/greatpic, users={detail=[{added=Sun Dec 22 2013 16:21:51 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Mon Mar 03 2014 22:20:46 +0000, family_name=Arredondo, screen_name=elianaa, given_name=Eliana, url=http://about.me/elianaa}, {added=Tue Mar 11 2014 17:59:27 +0000, family_name=Vlaskalic, screen_name=gianlucavlaskalic, given_name=Gianluca, url=http://about.me/gianlucavlaskalic}, {added=Tue Aug 19 2014 18:40:40 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}], size=4}, created=Sun Dec 22 2013 16:21:51 +0000, name=Pages with Great Pics, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/greatpic}, {id=jessica_tenny/impressed, users={detail=[{added=Thu Nov 21 2013 01:54:09 +0000, family_name=VanBibber, screen_name=brittanyvanbibber, given_name=Brittany, url=http://about.me/brittanyvanbibber}, {added=Wed Jul 02 2014 17:46:42 +0000, family_name=Sacco, screen_name=shelby_sacco, given_name=Shelby, url=http://about.me/shelby_sacco}], size=2}, created=Thu Nov 21 2013 01:54:09 +0000, name=Impressive Pages, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/impressed}, {id=jessica_tenny/love, users={detail=[{added=Thu Sep 19 2013 03:50:55 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Dec 17 2013 15:56:51 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=2}, created=Thu Sep 19 2013 03:50:55 +0000, name=Pages I Love, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/love}, {id=jessica_tenny/twitterfriends, users={detail=[{added=Tue Sep 03 2013 18:26:31 +0000, family_name=Crowley, screen_name=dens, given_name=Dennis, url=http://about.me/dens}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Malik, screen_name=om, given_name=Om , url=http://about.me/om}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name=Tweney, screen_name=dylan20, given_name=Dylan, url=http://about.me/dylan20}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Limongello, screen_name=tomlimongello, given_name=Tom, url=http://about.me/tomlimongello}, {added=Tue Sep 03 2013 18:26:31 +0000, family_name= Olanoff, screen_name=drew, given_name=Drew, url=http://about.me/drew}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Scoble, screen_name=scobleizer, given_name=Robert, url=http://about.me/scobleizer}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Gannes, screen_name=lizgannes, given_name=Liz, url=http://about.me/lizgannes}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Bradshaw, screen_name=timbr, given_name=Tim, url=http://about.me/timbr}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Silva, screen_name=christs, given_name=Chris, url=http://about.me/christs}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Wauters, screen_name=robinwauters, given_name=Robin, url=http://about.me/robinwauters}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Snell, screen_name=jsnell, given_name=Jason, url=http://about.me/jsnell}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McLoughlin, screen_name=robmcloughlin, given_name=Rob, url=http://about.me/robmcloughlin}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Biggs, screen_name=johnbiggs, given_name=John, url=http://about.me/johnbiggs}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Perez, screen_name=sarahperez, given_name=Sarah, url=http://about.me/sarahperez}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Steele, screen_name=lock, given_name=Lockhart, url=http://about.me/lock}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=, screen_name=adland, given_name=Adland, url=http://about.me/adland}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McCarthy, screen_name=carolinemccarthy, given_name=Caroline, url=http://about.me/carolinemccarthy}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Hesseldahl, screen_name=ahess247, given_name=Arik, url=http://about.me/ahess247}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Ingram, screen_name=mathewingram, given_name=Mathew, url=http://about.me/mathewingram}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Tai, screen_name=billtai, given_name=Bill, url=http://about.me/billtai}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Sullivan, screen_name=dsullivan, given_name=Danny, url=http://about.me/dsullivan}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= De Rosa, screen_name=anthonyderosa, given_name=Anthony, url=http://about.me/anthonyderosa}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Peretti, screen_name=jonah, given_name=Jonah, url=http://about.me/jonah}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lieb, screen_name=rebeccalieb, given_name=Rebecca, url=http://about.me/rebeccalieb}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Berkowitz, screen_name=dberkowitz, given_name=David, url=http://about.me/dberkowitz}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Bilton, screen_name=nickbilton, given_name=Nick, url=http://about.me/nickbilton}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Schafer, screen_name=ian, given_name=Ian, url=http://about.me/ian}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Thomas, screen_name=owenthomas, given_name=Owen, url=http://about.me/owenthomas}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Maverick, screen_name=kmaverick, given_name=Kristin, url=http://about.me/kmaverick}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lacy, screen_name=sarahlacy, given_name=Sarah, url=http://about.me/sarahlacy}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Morrissey, screen_name=bmorrissey, given_name=Brian, url=http://about.me/bmorrissey}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=ha, screen_name=thepeterha, given_name=peter, url=http://about.me/thepeterha}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Sterling, screen_name=gregsterling, given_name=Greg, url=http://about.me/gregsterling}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Shine, screen_name=benshine, given_name=Benjamin, url=http://about.me/benshine}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Clayman, screen_name=gclayman, given_name=Greg, url=http://about.me/gclayman}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Wilhelm, screen_name=alexwilhelm, given_name=Alex, url=http://about.me/alexwilhelm}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Jean, screen_name=dorothyjean, given_name=Dorothy, url=http://about.me/dorothyjean}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Tepley, screen_name=tepley, given_name=Allison, url=http://about.me/tepley}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Lazerow, screen_name=lazerow, given_name=Michael, url=http://about.me/lazerow}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Galloway, screen_name=scottgalloway, given_name=Scott, url=http://about.me/scottgalloway}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Van Grove, screen_name=jbruin, given_name=Jennifer, url=http://about.me/jbruin}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Parr, screen_name=benparr, given_name=Benjamin E., url=http://about.me/benparr}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Walsh, screen_name=mark_walsh, given_name=Mark, url=http://about.me/mark_walsh}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Kaufman, screen_name=michakaufman, given_name=Micha, url=http://about.me/michakaufman}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Steel , screen_name=emilysteel, given_name=Emily, url=http://about.me/emilysteel}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=, screen_name=nmcglynn, given_name=Nick, url=http://about.me/nmcglynn}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Eldon, screen_name=eric.eldon, given_name=Eric Rosser, url=http://about.me/eric.eldon}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Conyers, screen_name=aprilconyers, given_name=April, url=http://about.me/aprilconyers}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Hammerling, screen_name=brookehammerling, given_name=Brooke, url=http://about.me/brookehammerling}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Chen, screen_name=spencerchen, given_name=Spencer, url=http://about.me/spencerchen}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Gallop, screen_name=cindygallop, given_name=Cindy, url=http://about.me/cindygallop}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Thione, screen_name=thione, given_name=Lorenzo, url=http://about.me/thione}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Baratz, screen_name=mbaratz, given_name=Maya, url=http://about.me/mbaratz}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Barber, screen_name=nickbarber, given_name=Nick, url=http://about.me/nickbarber}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= Yeung, screen_name=kenyeung, given_name=Ken, url=http://about.me/kenyeung}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name=Tracte, screen_name=stwo, given_name=Stuart, url=http://about.me/stwo}, {added=Tue Sep 03 2013 18:26:32 +0000, family_name= McKenzie, screen_name=hamish, given_name=Hamish, url=http://about.me/hamish}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Haot, screen_name=rachelsterne, given_name=Rachel, url=http://about.me/rachelsterne}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Barganier, screen_name=laurabarganier, given_name=Laura, url=http://about.me/laurabarganier}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Lawler, screen_name=ryanlawler, given_name=Ryan, url=http://about.me/ryanlawler}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Warren, screen_name=christina, given_name=Christina, url=http://about.me/christina}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Shampine, screen_name=mjshampine, given_name=Matthew, url=http://about.me/mjshampine}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Wortham , screen_name=jennydeluxe, given_name=Jenna, url=http://about.me/jennydeluxe}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Teicher, screen_name=aerocles, given_name=David, url=http://about.me/aerocles}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Finnegan, screen_name=justinfinnegan, given_name=Justin, url=http://about.me/justinfinnegan}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Popper, screen_name=benpopper, given_name=Ben, url=http://about.me/benpopper}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Gannett, screen_name=emilygannett, given_name=Emily, url=http://about.me/emilygannett}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Kovach, screen_name=stevekovach, given_name=Steve, url=http://about.me/stevekovach}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Tong, screen_name=briantong, given_name=Brian, url=http://about.me/briantong}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Muradali, screen_name=sashamuradali, given_name=Sasha H., url=http://about.me/sashamuradali}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Schonfeld, screen_name=erickschonfeld, given_name=Erick, url=http://about.me/erickschonfeld}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Akselrud, screen_name=tracyakselrud, given_name=Tracy, url=http://about.me/tracyakselrud}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Tsotsis, screen_name=alexiatsotsis, given_name=Alexia, url=http://about.me/alexiatsotsis}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Waxler, screen_name=cwaxler, given_name=Caroline, url=http://about.me/cwaxler}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Isaac, screen_name=mike.isaac, given_name=Mike, url=http://about.me/mike.isaac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Johnson, screen_name=jcjohnson, given_name=Jason, url=http://about.me/jcjohnson}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Chowdhury, screen_name=rezac, given_name=Reza, url=http://about.me/rezac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Schulte, screen_name=erinschulte, given_name=Erin, url=http://about.me/erinschulte}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Huffington, screen_name=ariannahuffington, given_name=Arianna, url=http://about.me/ariannahuffington}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Brody, screen_name=jacobbrody, given_name=Jacob, url=http://about.me/jacobbrody}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Vogel, screen_name=neilvogel, given_name=Neil, url=http://about.me/neilvogel}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Lazar, screen_name=justjon, given_name=Jon, url=http://about.me/justjon}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fitzpatrick, screen_name=alexjfitzpatrick, given_name=Alex, url=http://about.me/alexjfitzpatrick}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Blount, screen_name=hagan, given_name=Hagan, url=http://about.me/hagan}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Kim, screen_name=oryankim, given_name=Ryan , url=http://about.me/oryankim}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Battelle, screen_name=johnbattelle, given_name=John, url=http://about.me/johnbattelle}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Jarvey, screen_name=nataliejarvey, given_name=Natalie, url=http://about.me/nataliejarvey}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Wohlsen, screen_name=marcuswohlsen, given_name=Marcus, url=http://about.me/marcuswohlsen}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Hightower, screen_name=takihigh, given_name=Takiyah, url=http://about.me/takihigh}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Del Rey, screen_name=delrey, given_name=Jason, url=http://about.me/delrey}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Arrington, screen_name=mike, given_name=Michael, url=http://about.me/mike}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Gallagher, screen_name=davidfg, given_name=David, url=http://about.me/davidfg}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Biddle, screen_name=samfbiddle, given_name=Sam, url=http://about.me/samfbiddle}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Faircloth, screen_name=kellyfaircloth, given_name=Kelly, url=http://about.me/kellyfaircloth}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Granzella Larssen, screen_name=adrian.granzella.larssen, given_name=Adrian, url=http://about.me/adrian.granzella.larssen}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Carpanzano, screen_name=caitlyncarpanzano, given_name=Caitlyn, url=http://about.me/caitlyncarpanzano}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Dellinger, screen_name=ajdellinger, given_name=AJ, url=http://about.me/ajdellinger}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Dalton, screen_name=ddalton, given_name=Doug, url=http://about.me/ddalton}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fiegerman, screen_name=sfiegerman, given_name=Seth, url=http://about.me/sfiegerman}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=, screen_name=conduityoursite, given_name=Conduit, url=http://about.me/conduityoursite}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= MacMillan, screen_name=dougmac, given_name=Doug, url=http://about.me/dougmac}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Nice Rob, screen_name=naughtybutnicerob, given_name=Naughty But, url=http://about.me/naughtybutnicerob}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name=Heussner, screen_name=kimaeheussner, given_name=Ki Mae, url=http://about.me/kimaeheussner}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Bonnington, screen_name=redgirl, given_name=Christina, url=http://about.me/redgirl}, {added=Tue Sep 03 2013 18:26:33 +0000, family_name= Fried, screen_name=ina, given_name=Ina, url=http://about.me/ina}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Griffith, screen_name=eringriffith, given_name=Erin, url=http://about.me/eringriffith}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Carr, screen_name=austincarr, given_name=Austin, url=http://about.me/austincarr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Yarow, screen_name=jay.yarow, given_name=Jay, url=http://about.me/jay.yarow}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Kern, screen_name=elizakern, given_name=Eliza, url=http://about.me/elizakern}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Cook, screen_name=denacook, given_name=Dena, url=http://about.me/denacook}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Segall, screen_name=lauriesegall, given_name=Laurie, url=http://about.me/lauriesegall}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Barna, screen_name=kathbarna, given_name=Katherine, url=http://about.me/kathbarna}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Constine, screen_name=joshconstine, given_name=Josh, url=http://about.me/joshconstine}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Indvik, screen_name=laurenindvik, given_name=Lauren, url=http://about.me/laurenindvik}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Warzel, screen_name=charliewarzel, given_name=Charlie, url=http://about.me/charliewarzel}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Lunden, screen_name=ingrid.lunden, given_name=Ingrid, url=http://about.me/ingrid.lunden}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Raymond, screen_name=hraymond, given_name=Harry, url=http://about.me/hraymond}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Goode, screen_name=laurengoode, given_name=Lauren, url=http://about.me/laurengoode}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Carney, screen_name=mcarney, given_name=Michael, url=http://about.me/mcarney}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Banda, screen_name=jackieb3, given_name=Jackie, url=http://about.me/jackieb3}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Dickey, screen_name=meganrosedickey, given_name=Megan Rose, url=http://about.me/meganrosedickey}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Rick, screen_name=christophorrick, given_name=Christophor, url=http://about.me/christophorrick}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Glenn, screen_name=devonglenn, given_name=Devon, url=http://about.me/devonglenn}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Wang, screen_name=jenniferwang, given_name=Jennifer, url=http://about.me/jenniferwang}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Prichard, screen_name=meghanprichard, given_name=Meghan, url=http://about.me/meghanprichard}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Campisano, screen_name=kcampisano, given_name=Katie, url=http://about.me/kcampisano}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Walton        , screen_name=elizabethwalton, given_name=Liz, url=http://about.me/elizabethwalton}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Sonneland, screen_name=hsonneland, given_name=Haley, url=http://about.me/hsonneland}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Whitfield, screen_name=aaron.whitfield, given_name=Aaron, url=http://about.me/aaron.whitfield}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Ahuja, screen_name=maneet, given_name=Maneet, url=http://about.me/maneet}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Rosario, screen_name=elisabethmrosario, given_name=Elisabeth, url=http://about.me/elisabethmrosario}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Lai, screen_name=jessicaclai, given_name=Jessica, url=http://about.me/jessicaclai}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=, screen_name=aboutmeteam, given_name=About.me, url=http://about.me/aboutmeteam}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Stone, screen_name=brittanystone, given_name=Brittany, url=http://about.me/brittanystone}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Hamburger, screen_name=ellishamburger, given_name=Ellis, url=http://about.me/ellishamburger}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Hockenson, screen_name=laurenhockenson, given_name=Lauren, url=http://about.me/laurenhockenson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Bea, screen_name=francisybea, given_name=Francis, url=http://about.me/francisybea}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=thompson, screen_name=cadiethompson, given_name=cadie, url=http://about.me/cadiethompson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Hammerling, screen_name=haley_hammerling, given_name=Haley, url=http://about.me/haley_hammerling}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Farr, screen_name=christinafarr, given_name=Christina, url=http://about.me/christinafarr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Minshew, screen_name=kminshew, given_name=Kathryn, url=http://about.me/kminshew}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Colao, screen_name=jjcolao, given_name=J.J., url=http://about.me/jjcolao}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Tomaeno, screen_name=elliotpr, given_name=Elliot, url=http://about.me/elliotpr}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=DeAmicis, screen_name=cdeamicis, given_name=Carmel, url=http://about.me/cdeamicis}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Cohen Blatter, screen_name=lucy.blatter, given_name=Lucy, url=http://about.me/lucy.blatter}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Wauck, screen_name=kwauck, given_name=Kate, url=http://about.me/kwauck}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Marsicano, screen_name=john.marsicano, given_name=John, url=http://about.me/john.marsicano}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=Jackson, screen_name=joseph_jackson, given_name=Joseph, url=http://about.me/joseph_jackson}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name= Smith, screen_name=kevinlsmith, given_name=Kevin, url=http://about.me/kevinlsmith}, {added=Tue Sep 03 2013 18:26:34 +0000, family_name=fuller, screen_name=kiyomifuller, given_name=kiyomi, url=http://about.me/kiyomifuller}], size=151}, created=Tue Sep 03 2013 18:26:28 +0000, name=Twitter Friends, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/twitterfriends}, {id=jessica_tenny/wellwritten, users={detail=[{added=Tue Dec 24 2013 04:34:36 +0000, family_name=Mann, screen_name=alextmann, given_name=Alex, url=http://about.me/alextmann}, {added=Wed Jan 08 2014 03:16:43 +0000, family_name=Shenoy, screen_name=julianshenoy, given_name=Julian, url=http://about.me/julianshenoy}, {added=Fri Apr 11 2014 04:08:14 +0000, family_name=Klosterman, screen_name=courtney.klosterman, given_name=Courtney, url=http://about.me/courtney.klosterman}, {added=Tue Aug 19 2014 18:40:12 +0000, family_name=Lampugnano, screen_name=jlampugnano, given_name=Jackie, url=http://about.me/jlampugnano}], size=4}, created=Tue Dec 24 2013 04:34:36 +0000, name=Pages with Awesome Bios, owner=jessica_tenny, url=http://about.me/jessica_tenny/collections/wellwritten}], size=10}");
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
